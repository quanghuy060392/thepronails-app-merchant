import React from "react";
import {
    Platform,
    StatusBar,
    StyleSheet,
    View,
    Image,
    AsyncStorage,
    Alert,
    AppState
} from "react-native";
import { LinearGradient, ScreenOrientation } from "expo";
import { NavigationProvider, StackNavigation } from "@expo/ex-navigation";
import Router from "./navigation/Router";
import cacheAssetsAsync from "./utilities/cacheAssetsAsync";
//import Spinner from "./helpers/loader";
import { isLogged, refreshJWTtoken, jwtToken, getUserData } from "./helpers/authenticate";
import SpinnerLoader from "./helpers/spinner";
import { getLanguageName, getLanguage, getTextByKey } from "./helpers/language";
//import CardConnectHelper from "./CardConnectNativeModule";
if(Platform.OS != "ios"){
    ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_LEFT);
}else{
    ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_RIGHT);
}
export default class App extends React.Component {
    defaultRoute = "rootNavigation";
    isNotification = false;
    languageKey = '';
    businessname = "";
    state = {
        appIsReady: false,
        isCheckAuthenticate: false,
        //appState: AppState.currentState
    };
    /*
    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            //console.log(this.state.appState)
        }
        this.setState({appState: nextAppState});
    }*/

    async componentWillMount() {
        if(Platform.OS != "ios"){
            ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_LEFT);
        }
        
        setTimeout(function() {
            if(Platform.OS === "ios"){
                ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_RIGHT);
            }
            
        }, 0);
        //console.log(await CardConnect.exampleMethod());
        //console.log(CardConnect);
        /*
        CardConnectHelper.exampleMethod((str) => {
            console.log(str);
        })*/
        //CardConnectHelper.connect();
        let isLoggedIn = await isLogged();
        if (isLoggedIn) {
            let token = await jwtToken();
            await refreshJWTtoken(token);
            this.defaultRoute = "home";
            this.userData = await getUserData();
            this.businessname = this.userData.businessname;
        } else this.defaultRoute = "login";

        this._loadAssetsAsync();
        this.languageKey = await getLanguage();
        this.setState({ isCheckAuthenticate: true });
        this.setState({ appIsReady: true });
       // this.refs.CardConnect.connect();
        
        //this._loadAssetsAsync();
    }

    async _loadAssetsAsync() {
        try {
            await cacheAssetsAsync({
                images: [require("./assets/images/expo-wordmark.png")],
                fonts: [
                    {
                        "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf")
                    },                    
                    {
                        "heavenmatters": require("./assets/fonts/heavenmatters.ttf")
                    },
                    {
                        "NeotericcRegular": require("./assets/fonts/NeotericcRegular.ttf")
                    },
                    {
                        "Futura": require("./assets/fonts/Futura3.ttf")
                    },
                    {
                        "futuralight": require("./assets/fonts/FuturaStdLight.ttf")
                    },
                    {
                        "angel": require("./assets/fonts/AngelTears.ttf")
                    }
                ]
            });
        } catch (e) {
            console.warn(
                "There was an error caching assets (see: main.js), perhaps due to a " +
                    "network timeout, so we skipped caching. Reload the app to try again."
            );
            console.log(e.message);
        } finally {
            setTimeout(() => {
                this.setState({ appIsReady: true });
            }, 500);
        }
    }

    render() {
        if (this.state.appIsReady && this.state.isCheckAuthenticate) {
            return (
                <View style={styles.container}>
                   
               {/*  <CardConnectHelper ref="CardConnect" /> */}
                   <NavigationProvider router={Router}>
                        <StackNavigation
                            id="root"
                            initialRoute={Router.getRoute(this.defaultRoute,{language: this.languageKey, businessname: this.businessname })}
                        />
                    </NavigationProvider>

                    {Platform.OS === "ios" &&
                        <StatusBar barStyle="light-content" />}
                    {Platform.OS === "android" &&
                        <View style={styles.statusBarUnderlay} />}
                    
                </View>
            );
        } else {
            return (
                <LinearGradient
                    start={[0, 0]}
                    end={[1, 1]}
                    colors={["#F069A2", "#EECBA3"]}
                    style={styles.loader}
                >
                    <SpinnerLoader
                        visible={true}
                        textStyle={styles.textLoader}
                        overlayColor={"transparent"}
                        textContent={"Loading..."}
                    />
                </LinearGradient>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    statusBarUnderlay: {
        height: 24,
        backgroundColor: "rgba(0,0,0,0.2)"
    },
    textLoader: {
        color: "#fff",
        fontWeight: "normal",
        fontSize: 16
    },
    loader: {
        flex: 1
    },
    Maintainloader: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    maintaintext:{
        color:'#fff',
        fontSize:22
    }
});
