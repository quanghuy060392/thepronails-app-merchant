import React from "react";
import {
    StyleSheet,
    Text,
    View,
    Platform,
    TouchableOpacity,
    AsyncStorage,
    Alert,
    ScrollView
} from "react-native";
import { LinearGradient, Notifications, Permissions } from "expo";
import { isLogged, jwtToken, getUserData } from "../helpers/authenticate";
import { StackNavigation } from "@expo/ex-navigation";
import SpinnerLoader from "../helpers/spinner";
import Colors from "../constants/Colors";
import layout from "../assets/styles/layout";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import setting from "../constants/Setting";
import Router from "../navigation/Router";
import FloatSelectInput from "../components/FloatSelectInput";
import { getProfileData } from "../api/getProfileData";
import FloatLabelInput from "../components/FloatLabelInput";
import DetailProfileScreen from '../screens/DetailProfileScreen';
import NavigationBarBackground from "../components/navigationBarBG";
import NavigationBarTitle from "../components/navigationBarTitle";
import FloatLabelSelect from "../components/FloatSelectInputPriceMultiple";
import { getLanguageName, getLanguage, getTextByKey } from "../helpers/language";
import LanguageModal from "../components/LanguageModal";
import IconLoader from "../helpers/iconloader";

export default class MoreScreen extends React.Component {
    languageKey = typeof(this.props.route.params.language) != 'undefined' ? this.props.route.params.language : 'en-US';

    static route = {
        navigationBar: {
            visible: true,
            elevation: 0,
            renderBackground: () => {
                return <NavigationBarBackground />;
            },
            renderTitle: route => {
                return (
                    <NavigationBarTitle
                        title={'morenav'}
                        language={route.params.language}
                    />
                );
            }
        }
    };

    state = {
        appIsReady: false
    };

    languageName = '';
    

    async componentWillMount() {
        

        this.languageName = await getLanguageName();
        this.languageKey = await getLanguage();
        this.userData = await getUserData();
        /*
        this.isLoggedIn = await isLogged();
        if (this.isLoggedIn) {
            this.token = await jwtToken();
            this.userData = await getUserData();
            this.fullname = this.userData.businessname;
            if (this.userData.role == 9) {
                this.fullname = this.userData.fullname;
            }
            this.profile = await getProfileData(this.token);
            //console.log(this.profile);
        } else {
            this.props.navigator.push("login");
        }
        this.setState({ appIsReady: true });*/

        this.setState({ appIsReady: true });

        if(typeof(this.props.route.params.isSetLanguage) != 'undefined'){
            this.refs.appointmentSuccessLoader.setState({
                textContent: 'Language Applied',
                visible: true
            });
            //console.log(responseJson.data);    
            let _this = this;
            setTimeout(function() {
                _this.refs.appointmentSuccessLoader.setState({
                    visible: false
                });
               
            }, 2000);
        }
    }
    
    async logout() {
        AsyncStorage.removeItem("salonlogin");
        let rootNavigator = this.props.navigation.getNavigator("root");
        rootNavigator.replace(Router.getRoute("home",{businessname: this.userData.businessname}));
    }

    /*
    showPayouts = () => {
        let rootNavigator = this.props.navigation.getNavigator("root");
        rootNavigator.push(Router.getRoute('payout'));
    }

    edit = () => {
        //this.setState({ data: this.profile, modalVisible: true });
        //this.refs.DetailProfileScreen.data = this.profile;
        this.refs.DetailProfileScreen.show(this.profile);
        //let rootNavigator = this.props.navigation.getNavigator("root");
        //rootNavigator.push(Router.getRoute('detail_profile',{data: this.profile, userData: this.userData}));
    }

    SaveSuccess = (data) => {
        this.profile = data;
        this.refs.DetailProfileScreen.close();
        this.setState({ appIsReady: true });
    }*/

    onPressLanguage = () => {
        //this.props.onPress(id, refdata);
        //console.log('ok');
        this.refs.LanguageModal.show(this.languageKey);
    };

    onSelectLanguage = (languageName,languageKey) => {
        this.languageKey = languageKey;
        this.languageName = languageName;
        this.refs.LanguageModal.close();
        let _this = this;
        //let rootNavigator = this.props.navigation.getNavigator("root");
        //this.props.navigation.immediatelyResetStack([Router.getRoute('rootNavigation',{language: this.languageKey})], 0);
        //rootNavigator.replace(Router.getRoute('rootNavigation',{language: this.languageKey}));
        
        /*
        this.setState({ rerender:true });

        let _this = this;
        _this.props.navigation.performAction(
            ({ tabs, stacks }) => {
               
                tabs("maintab").jumpToTab('clients');
                //tabs("maintab").jumpToTab('more');
              
            }
        );*/
        let rootNavigator = this.props.navigation.getNavigator("root");
        rootNavigator.replace(Router.getRoute('applyLanguage',{language: _this.languageKey,changeLanguage: true}));

        _this.props.navigation.performAction(
            ({ tabs, stacks }) => {
                //stacks('more').popToTop('more');    
                //const { currentNavigatorUID } = _this.props.navigation.navigationState;
                //console.log(currentNavigatorUID);
                //let rootNavigator = _this.props.navigation.getNavigator('more');
                //rootNavigator.replace(Router.getRoute('more',{language: _this.languageKey}));
                //stacks("root").replace(Router.getRoute('rootNavigation',{language: _this.languageKey}));
                //tabs("maintab").immediatelyResetStack([Router.getRoute('more',{language: _this.languageKey})], 0);
                //tabs("maintab").jumpToTab('more');
                //let rootNavigator = _this.props.navigation.getNavigator('root');
                //rootNavigator.pop(1);
                //stacks('more').push('more');
                //rootNavigator.immediatelyResetStack([Router.getRoute('rootNavigation',{language: _this.languageKey})], 0);
                //var stackOfRoutes = this.props.navigator._getNavigatorState().routes;
                //console.log(stackOfRoutes);
            }
        );
    }

    render() {
        if (this.state.appIsReady) {
            return (
                <View style={styles.container}>
                    
                    {/*                        
                    <ScrollView style={styles.container}>
                        {this.userData.role == 4 &&
                            <FloatSelectInput
                                placeholder={""}
                                value="Payouts"
                                onPress={this.showPayouts}
                            />}
                    </ScrollView>
                    */}
                    <ScrollView style={styles.container}>
                        <View style={[layout.floatGroup,styles.btniconleftcontainer]}>
                            <MaterialCommunityIcons
                                name={"web"}
                                size={24}
                                color={
                                    "#bfbfbf"
                                }
                                style={
                                   [
                                    Platform.OS ===
                                    "android"
                                        ? layout.navIcon
                                        : layout.navIconIOS,
                                        styles.btniconleft
                                   ] 
                                }
                            />
                            <FloatLabelSelect
                                placeholder={this.languageName}
                                value={getTextByKey(this.languageKey,'language')}
                                onPress={this.onPressLanguage}
                                paddingStyle={styles.iconleftview}
                            />
                        </View>

                        <TouchableOpacity
                            style={styles.btnLogout}
                            activeOpacity={1}
                            onPress={async () => await this.logout()}
                        >
                            <View>
                                <Text style={styles.btnLogoutText}>
                                    {getTextByKey(this.languageKey,'logout')}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </ScrollView>

                    <SpinnerLoader
                        visible={false}
                        textStyle={layout.textLoaderScreen}
                        overlayColor={"rgba(255,255,255,0.3)"}
                        textContent={"Logging Out..."}
                        color={Colors.spinnerLoaderColor}
                        ref="spinner"
                    />

                    <LanguageModal 
                        visible={false} 
                        ref="LanguageModal"
                        onPress={this.onSelectLanguage}
                    />   

                    <IconLoader
                        ref="appointmentSuccessLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmitSucccess}
                        textContent={"Appointment Booked"}
                        color={Colors.spinnerLoaderColorSubmit}
                    />                 
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <SpinnerLoader
                        visible={true}
                        textStyle={layout.textLoaderScreen}
                        overlayColor={"transparent"}
                        textContent={"Loading..."}
                        color={Colors.spinnerLoaderColor}
                    />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#f2f2f2'
    },
    profilepicture: {
        marginTop: 30
    },
    btnLogout: {
        height:50,
        backgroundColor:'#fff',
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginTop: 25,
        borderWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd',
    },
    btnLogoutText:{
        color:'#F069A2'
    },
    btniconleft:{
        position:'absolute',
        left:5,
        backgroundColor:'transparent'
    },
    iconleftview:{
        paddingLeft:35,
        
    },
    btniconleftcontainer:{
        justifyContent:'center'
    }
});
