import React from "react";
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Alert,
    AsyncStorage,
    Dimensions,
    Keyboard
} from "react-native";
import {Expo, LinearGradient} from "expo";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import btn from "../assets/styles/button";
import layout from "../assets/styles/layout";
import { getBackground, getLogo } from "../api/Assets";
import Colors from "../constants/Colors_checkin";
import BtnSpinner from "../helpers/btnloader";
import emailvalidator from "email-validator";
import setting from "../constants/Setting";
import Router from "../navigation/Router";
import { StackNavigation } from "@expo/ex-navigation";
import registerForPushNotificationsAsync from "../api/registerForPushNotificationsAsync";
import {  getDeviceId } from "../helpers/authenticate";
import { getLanguageName, getLanguage, getTextByKey } from "../helpers/language";
import {
    jwtToken,
    getUserData
} from "../helpers/authenticate";
import SubmitLoader from "../helpers/submitloader";
import ImageResponsive from 'react-native-scalable-image';
var {height, width} = Dimensions.get('window');
export default class SelectAppScreen extends React.Component {
    static route = {
        navigationBar: {
            visible: false
        },
    };
    state = {
        appIsReady: false,
        userData: ""
    };

    logo_app = "";
    async componentWillMount(){
        var screen = Dimensions.get('window');
        width = screen.width;
        height = screen.height;
        this.languageKey = await getLanguage();
        let userData = await getUserData();

        let logo_app = await getLogo();
        if(logo_app != '' && logo_app != null){
            this.logo_app = logo_app;
        }
        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            height = screen.height;
        });
        this.setState({ userData: userData });
    }
    async merchant(){
        this.props.navigator.push(Router.getRoute('rootNavigation',{language: this.languageKey}));
    }
    async checkin (){
        this.props.navigator.push(Router.getRoute('home',{businessname: this.state.userData.businessname}));
    }
    render() {
        if(this.state.userData != ""){
            let logostyle = styles.logofont;

            if(this.state.userData.businessname.indexOf('&') >= 0){
                logostyle = styles.logofontAngel;
            }
            return (
                <View style={{flex:1}}>
                <Image 
                        source={require('../assets/images/nailsbgv2.png')}
                        style={{width:width,height:height}}
                    /> 
                <LinearGradient start={[0, 0]} end={[1, 1.0]} colors={["rgba(255,255,255,0.8)", "rgba(255,255,255,0.8)"]} style={[styles.containerGradient,{width:width,height:height}]}>
                    <Text style={styles.welcome}>Welcome to</Text> 
                        <View style={{ position:'absolute',top:50,left:50,zIndex:2}}>
                                        {this.logo_app != '' &&
                                            <ImageResponsive 
                                                source={{uri:this.logo_app}}
                                                height={59}
                                                style={[styles.logo,{width:100,  }]}
                                            />
                                        }
    
                                        {this.logo_app == '' &&
                                            <Text style={[styles.logofontdefault,logostyle]}>{this.state.userData.businessname}</Text>
                                        }
                        </View>
                    <View style={styles.maincontainer}>
                            <View style={styles.forgotpass}>
                                <View style={{marginRight:200, alignItems:'center',justifyContent:'center'}}>
                                    <TouchableHighlight activeOpacity={1}  onPress={async () => {await this.merchant()}}>
                                        <Image source={require('../assets/icons/icon-app-new.png')} style={{width: 150, height: 150}} />
                                    </TouchableHighlight>
                                    <Text style={{fontSize:25, color:"#F069A2",fontFamily:'Futura', marginTop:20}}>Merchant App</Text>
                                </View>
                                <View style={{alignItems:'center',justifyContent:'center'}}>
                                    <TouchableHighlight activeOpacity={2} onPress={async () => {await this.checkin()}}>
                                        <Image source={require('../assets/icons/appiconcheckin.png')} style={{width: 150, height: 150}} />
                                    </TouchableHighlight>
                                    <Text style={{fontSize:25,fontFamily:'Futura', color:"#F069A2", marginTop:20}}>Check-In App</Text>
                                </View>
                            </View>
                    </View>

                </LinearGradient>   
    
            </View>
            );
        }else{
            return(
                <View style={{flex:1}}>
                    <Image 
                            source={require('../assets/images/nailsbgv2.png')}
                            style={{width:width,height:height}}
                        /> 
                    <LinearGradient start={[0, 0]} end={[1, 1.0]} colors={["rgba(241,82,149,0.8)", "rgba(241,82,149,0.05)"]} style={[styles.containerGradient,{width:width,height:height}]}>

                        <SubmitLoader
                        ref="authenticateLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={"Processing..."}
                        color={Colors.spinnerLoaderColorSubmit}
                    />
                    </LinearGradient>   
                    
                </View>
            )  
        }
    }
}
const styles = StyleSheet.create({
    containerGradient: {
        flex: 1,
        opacity:0.8,
        position:'absolute',
        top:0,
        bottom:0,
        zIndex:1
    },
    maincontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    welcome:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#333',
        fontFamily: 'futuralight',
        marginBottom:0,
        position:'absolute',top:30,left:50
    },
    logofontdefault:{
        backgroundColor:'transparent',
        color:'#fff',
        fontSize:70,
        marginBottom:30,
        textAlign:'center',
        paddingLeft:20,
        paddingRight:20
    },
    logofont:{
        fontFamily: 'NeotericcRegular'
    },
    logofontAngel:{
        fontFamily: 'angel'
    },
    logo:{
        marginBottom:30
    },


    hrLeft: {
        borderBottomColor: '#fff',
        borderBottomWidth: 0.5,
        //marginTop:10,
        position: 'absolute',
        zIndex: 1,
        width: 110,
        left: 10
    },
    hrRight: {
        borderBottomColor: '#fff',
        borderBottomWidth: 0.5,
        //marginTop:10,
        position: 'absolute',
        zIndex: 1,
        width: 110,
        left: 150
    },
    hrContent: {
        position: 'absolute',
        zIndex: 2,
        fontSize: 13,
        paddingLeft: 5,
        paddingRight: 5,
        color: '#fff',
        backgroundColor: 'transparent'
    },
    inlineicon: {
        color: 'rgba(255,255,255,0.5)',
        position: 'absolute',
        top: 7,
        left: 10,
        backgroundColor: "transparent",
        zIndex: 1
    },
    txtLoginForm: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        borderRadius: 20,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 40,
        paddingRight: 10,
        color: '#fff'
    },

    loader: {
        flex: 1
    },
    forgotpass:{
       flexDirection: 'row',
    },
    contanerIos:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
})
