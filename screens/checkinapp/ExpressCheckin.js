import React from "react";
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Alert,
    AsyncStorage,
    Platform 
} from "react-native";
import { LinearGradient} from "expo";
import Customer from "../../components_checkin/clients/Customer";
import moment from 'moment';
import layout from "../../assets/styles/layout_checkin";
import Colors from "../../constants/Colors_checkin";
import SubmitLoader from "../../helpers/submitloader";
import setting from "../../constants/Setting";
import Router from "../../navigation/Router";
import {MaterialCommunityIcons} from "@expo/vector-icons";
export default class ExpreeeCheckInScreen extends React.Component{
    static route = {
        navigationBar: {
            visible: false
        }
    };
    userData = this.props.route.params.userData;
    isClientExists = this.props.route.params.isClientExists;
    clientSearchData = this.props.route.params.clientData;
    async componentWillMount(){

    }
    onPressClient = async (client)=> {
        this.clientData = client;
        //console.log(this.clientData);
        this.refs.appointmentLoader.setState({ visible: true }); 
        fetch(setting.apiUrl + "express-checkin/client", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.props.token
            },
            body: JSON.stringify(this.clientData)
        })
        .then(response => response.json())
        .then(responseJson => {
            if (!responseJson.success) {
                this.refs.appointmentLoader.setState({
                    visible: false
                });
                var _Alert = Alert;
                setTimeout(function(){
                    _Alert.alert('Error',responseJson.message);
                },200);
            } else {
                this.refs.appointmentLoader.setState({
                    visible: false
                });
                var _this = this;
                _this.props.navigator.push(Router.getRoute('CheckInSuccess',{logo_app: _this.props.route.params.logo_app}));
            }
        })
        .catch(error => {
            console.error(error);
        });
    }
    close = () => {
        this.props.navigator.push(Router.getRoute('home',{businessname: this.props.route.params.userData.businessname, isBooked: true, isShowStaffCheckIn: this.props.route.params.isShowStaffCheckIn,
            logo_app: this.props.route.params.logo_app}));
    }
    onClientUpdated = (client) => {
        console.log(1);
        this.clientData = client;
    }
    
    render(){
        let containerHeaderStepsStyle = styles.containerHeaderSteps;
        let headerStyle = styles.headerContainer;
        if(Platform.OS != 'ios'){
            headerStyle = styles.headerContainerAndroid;
            containerHeaderStepsStyle = styles.containerHeaderStepsAndroid;
        }
        return(
            <View style={styles.container}>
                <LinearGradient start={[0, 0]} end={[1, 1]} colors={['#F069A2', '#EEAEA2']} style={containerHeaderStepsStyle}>
                    <View>
                        <View style={headerStyle}>
                        <Text style={styles.headerTitle}>Profile Information</Text>
                        <TouchableOpacity style={styles.closebtn} activeOpacity={1}
                            onPress={this.close}>
                            <MaterialCommunityIcons
                                name={'close'}
                                size={30}
                                color={'rgba(255,255,255,1)'} style={styles.navIconIOS}
                            />
                        </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>     
                <Customer 
                    clientSearchData={this.clientSearchData} 
                    isClientExists={this.isClientExists} 
                    clients={this.props.route.params.clients} 
                    onPress={this.onPressClient} 
                    token={this.props.route.params.token} 
                    onClientUpdated={this.onPressClient}
                />
                <SubmitLoader
                    ref="appointmentLoader"
                    visible={false}
                    textStyle={layout.textLoaderScreenSubmit}
                    textContent={"Processing..."}
                    color={Colors.spinnerLoaderColorSubmit}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerHeaderSteps:{
        height:70
    },
    containerHeaderStepsAndroid:{
        height:185
    },
    headerContainer:{
        height:80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerContainerAndroid:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop:20
    },
    closebtn:{
        position:'absolute',
        right:20,
        backgroundColor:'transparent',
        top:30
    },
    headerTitle:{
        color:'#fff',
        backgroundColor:'transparent',
        fontSize:24,
        fontFamily:'Futura'
    },
})