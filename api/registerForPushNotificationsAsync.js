import { Permissions, Notifications } from 'expo';
import { AsyncStorage, Alert } from "react-native";
import setting from "../constants/Setting";
import { jwtToken } from "../helpers/authenticate";

// Example server, implemented in Rails: https://git.io/vKHKv
//const PUSH_ENDPOINT = 'https://exponent-push-server.herokuapp.com/tokens';
const PUSH_ENDPOINT = setting.apiUrl + 'expo/tokens';

export default (async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }

  // Get the token that uniquely identifies this device
  let token = await Notifications.getExpoPushTokenAsync();
  let usertoken = await jwtToken();

  // POST the token to our backend so we can use it to send pushes from there
  return await fetch(PUSH_ENDPOINT, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + usertoken,
    },
    body: JSON.stringify({
      token: token,
    }),
  }).then((response) => response.json()).then((responseJson) => {
      //console.log(responseJson);
      //Alert.alert('Error', JSON.stringify(responseJson));  
      if(responseJson.success){
        AsyncStorage.setItem(setting.deviceid,responseJson.deviceid.toString());
      }
  });
});
