import React from "react";
import {
    StyleSheet,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    TextInput
} from "react-native";
import layout from "../../assets/styles/layout_checkin";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import { LinearGradient, ScreenOrientation} from "expo";
import BtnQuantity from "../../components_checkin/btnQuantity";
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').width.height;
//var orientation = (width > height) ? 'LANDSCAPE' : 'PORTRAIT';
var columns = 2;
var serviceWidth = (width - 45) / columns;
var serviceRightWidth = 95;
var serviceLeftWidth = serviceWidth - serviceRightWidth;
//const orientation = (width > height) ? 'LANDSCAPE' : 'PORTRAIT';

export default class Listservices extends React.Component{
    state = {
        selectedService:[],
        showCloseSearchBox: false
    }

    columnWidth = width / columns;
    search = '';
    selectServices = [];
    price = 0;

    componentWillUnmount(){
        Dimensions.removeEventListener("change", () => {});
    }

    componentWillMount(){
        width = Dimensions.get('window').width;
        height = Dimensions.get('window').width.height;
        serviceWidth = (width - 45) / columns;
        serviceLeftWidth = serviceWidth - serviceRightWidth;
        this.columnWidth = width / columns;

        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            height = screen.height;
            /*
            orientation = (width > height) ? 'LANDSCAPE' : 'PORTRAIT';
            if(orientation == 'LANDSCAPE'){
                columns = 3;
            }*/

            serviceWidth = (width - 45) / columns;
            serviceLeftWidth = serviceWidth - serviceRightWidth;
            _this.columnWidth = width / columns;
            
            _this.setState({ appIsReady: true });
        })
    }

    changeSearchText = (searchtext) => {
        this.search = searchtext;
        if (String.prototype.trim.call(searchtext) == '') {
            this.setState({showCloseSearchBox: false});
        } else {
            this.setState({showCloseSearchBox: true});
        }
    }

    clearSearch = () => {
        this.search = '';
        this.refs['searchtextinput'].clear();
        this.setState({showCloseSearchBox: false});
    }

    onPressService = (service) => {
        //console.log(service);
        let serviceSelected = this.selectServices.filter(function(item){
            return item.id == service.id;
        });
        let newList = [];
        if(serviceSelected.length){
            this.selectServices.forEach(function(item) {
                if(item.id != service.id){
                    newList.push(item);
                }
            });
            this.selectServices = newList;
        }else{
            this.selectServices.push(service);
        }
        
        this.props.onPress(this.selectServices);  
        this.setState({selectedService: this.selectServices});      
    }

    setServices = (services) => {
        this.selectServices = services;
        //console.log(this.selectServices );
        this.setState({selectedService: this.selectServices}); 
    }

    onSelectedServices = () => {
        let isValid = Object.keys(this.selectServices).length;
        if(!isValid){
            Alert.alert('Error','Please choose service');        
        }else{
            this.props.onSelectedServices();  
        }
    }
    onQuantityService = (type, id) =>{

        let serviceItem = this.props.services.filter(function(item){
            return item.id == id;
        })[0];
        if(type == 'plus'){
            if(typeof(serviceItem.quantity) != 'undefined' && serviceItem.quantity > 1){
                serviceItem.quantity += 1;
            }else{
                serviceItem.quantity = 2;
            }
        }else{
            if(typeof(serviceItem.quantity) != 'undefined' && serviceItem.quantity > 1){
                serviceItem.quantity -= 1;
            }else{
                serviceItem.quantity = 1;
            }
        }
        this.setState({ appIsReady: true });
    }
    onQuantityCombo = (type, id) =>{

        let serviceItem = this.props.listcombo.filter(function(item){
            return item.id == id;
        })[0];
        if(type == 'plus'){
            if(typeof(serviceItem.quantity) != 'undefined' && serviceItem.quantity > 1){
                serviceItem.quantity += 1;
            }else{
                serviceItem.quantity = 2;
            }
        }else{
            if(typeof(serviceItem.quantity) != 'undefined' && serviceItem.quantity > 1){
                serviceItem.quantity -= 1;
            }else{
                serviceItem.quantity = 1;
            }
        }
        this.setState({ appIsReady: true });
    }
    renderServices = (category) => {
        let increase = 0;
        let serviceInCategories = this.props.services.filter(function(item){
            if(typeof(item.category_customname) != 'undefined' && String.prototype.trim.call(item.category_customname) != ''){
                return item.category_customname == category;
            }else{
                return item.category_name == category;
            }
            
        });
        let Service = false;
        let isAny = false;
        if(serviceInCategories.length){

            let firstService = serviceInCategories[0];
            if(typeof(this.props.userData) != 'undefined'){
                if(this.props.userData.serviceSortType == 'Alphabet'){
                    serviceInCategories = serviceInCategories.sort(function (a, b) {
                        if (a.service_name < b.service_name) return -1;
                        else if (a.service_name > b.service_name) return 1;
                        return 0;
                    });
                }else if(this.props.userData.serviceSortType == 'Price'){
                    serviceInCategories = serviceInCategories.sort(function (a, b) {
                        if (a.price < b.price) return -1;
                        else if (a.price > b.price) return 1;
                        return 0;
                    });
                }else{
                    serviceInCategories = serviceInCategories.sort(function (a, b) {
                        if (a.display_order < b.display_order) return -1;
                        else if (a.display_order > b.display_order) return 1;
                        return 0;
                    });
                }
            }
                

            Service = serviceInCategories.map((x, i) => {
                if(x.id.toString().indexOf('service') < 0){
                    x.id = 'service_' + x.id;
                }
                let quantity = 1;
                if(typeof(x.quantity) != 'undefined'){
                    quantity = x.quantity;
                }
                let serviceTextColor = styles.defaultColor;
                let serviceDurationTextColor = styles.defaultColor;
                let isExists = this.selectServices.filter(function(item){
                    return item.id == x.id
                }).length;
        
                if(isExists){
                    this.price += (x.price * quantity);
                    serviceTextColor = styles.whiteColor;
                    serviceDurationTextColor =  styles.whiteDurationColor;
                }

                let isShow = true;
                if(this.search != ''){
                    isShow = x.service_name.toLowerCase().indexOf(this.search.toLowerCase()) >= 0 || x.price == this.search;
                }

                if(isShow && x.status == 'NO'){
                    isShow = false;
                }


                if(isShow){
                    isAny = true;
                    increase = i % columns;
                    return (
                        <TouchableOpacity key={x.id} activeOpacity={1} onPress={() => {this.onPressService(x)}}>
                            <View style={{ width: this.columnWidth  }}>
                                { isExists == 0 &&
                                    <View style={[styles.serviceItem, i % columns == 0 ? styles.even : styles.odd]}>
                                        
                                        <View style={[styles.serviceLeft,{width: serviceLeftWidth}]}>
                                            <Text style={[styles.serviceName,serviceTextColor]}>{x.service_name} - ${x.price}</Text>
                                        </View>
                                        <View style={styles.serviceRight}>
                                            {/* <Text style={[styles.servicePrice,serviceTextColor]}></Text> */}
                                            <BtnQuantity quantity={quantity} id={x.id} onPress={this.onQuantityService} />
                                        </View>
                                        
                                        
                                    </View>
                                }

                                { isExists > 0 &&
                                    <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']} 
                                        style={[styles.containerItemSelected,i % 2 == 0 ? styles.even : styles.odd]}>
                                        <View style={[styles.serviceLeft,{width: serviceLeftWidth}]}>
                                            <Text style={[styles.serviceName,serviceTextColor]}>{x.service_name} - ${x.price}</Text>
                                        </View>
                                        <View style={styles.serviceRight}>
                                            {/* <Text style={[styles.servicePrice,serviceTextColor]}></Text> */}
                                            <BtnQuantity quantity={quantity} id={x.id} onPress={this.onQuantityService} selected={true}/>
                                        </View>
                                    </LinearGradient>  
                                }

                                
                            </View>
                        </TouchableOpacity>
                    )            
                }else return false;
                
            });
        }
        if(!isAny) Service = false;
        return Service;
    }

    renderServicesInCombo = () => {
        let arrKey = {};
        let categoryDisplay = this.props.categories.map((category, i) => {
            let service = this.renderServices(category);
            if(service != false){
                return (
                    <View key={category} style={{width: width}}>
                        <View style={[styles.categoryContainer,{width: width}]}>
                            <Text style={styles.categoryLabel}>{category}</Text>
                        </View>    
                        <View style={{flexDirection: 'row',flexWrap: 'wrap'}}>
                            {service}
                        </View>
                    </View>
                )
            }else {
                return false;
            }
            
        });
        return categoryDisplay;
    }

    render() {
       
        this.price = 0;
        let increase = 0;
        
        let categoryDisplay = this.renderServicesInCombo();
        

        if(increase == 0) increase = 1;
        else increase = 0;
        let isAnyCombo = false;
        if(typeof(this.props.userData) != 'undefined' && this.props.listcombo.length > 0){
            if(this.props.userData.serviceSortType == 'Alphabet'){
                this.props.listcombo = this.props.listcombo.sort(function (a, b) {
                    if (a.comboname < b.comboname) return -1;
                    else if (a.comboname > b.comboname) return 1;
                    return 0;
                });
            }else if(this.props.userData.serviceSortType == 'Price'){
                this.props.listcombo = this.props.listcombo.sort(function (a, b) {
                    if (a.price < b.price) return -1;
                    else if (a.price > b.price) return 1;
                    return 0;
                });
            }
        }
        let listcombo = this.props.listcombo.map((x, i) => {
            if(x.id.toString().indexOf('combo') < 0){
                x.id = 'combo_' + x.id;
            }
            let quantity = 1;
            if(typeof(x.quantity) != 'undefined'){
                quantity = x.quantity;
            }
            let serviceTextColor = styles.defaultColor;
            let serviceDurationTextColor = styles.defaultColor;
            let isExists = this.selectServices.filter(function(item){
                return item.id == x.id
            }).length;
      
            if(isExists){
                this.price += (x.price * quantity);
                serviceTextColor = styles.whiteColor;
                serviceDurationTextColor =  styles.whiteDurationColor;
            }

            let isShow = true;
            if(this.search != ''){
                isShow = x.comboname.toLowerCase().indexOf(this.search.toLowerCase()) >= 0 || x.price == this.search;
            }

            if(isShow){
                isAnyCombo = true;
                return (
                    <TouchableOpacity key={x.id} activeOpacity={1} onPress={() => {this.onPressService(x)}}>
                        <View style={{ width: this.columnWidth  }}>
                            { isExists == 0 &&
                                <View style={[styles.serviceItem, (i + increase) % columns == 0 ? styles.even : styles.odd]}>
                                    
                                    <View style={[styles.serviceLeft,{width: serviceLeftWidth}]}>
                                        <Text style={[styles.serviceName,serviceTextColor]}>{x.comboname} - ${x.price}</Text>
                                    </View>
                                    <View style={styles.serviceRight}>
                                            <BtnQuantity quantity={quantity} id={x.id} onPress={this.onQuantityCombo} />
                                    </View>
                                    
                                    
                                </View>
                            }

                            { isExists > 0 &&
                                <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']} 
                                    style={[styles.containerItemSelected,(i + increase) % 2 == 0 ? styles.even : styles.odd]}>
                                    <View style={[styles.serviceLeft,{width: serviceLeftWidth}]}>
                                        <Text style={[styles.serviceName,serviceTextColor]}>{x.comboname} - ${x.price}</Text>
                                    </View>
                                    <View style={styles.serviceRight}>
                                        <BtnQuantity quantity={quantity} id={x.id} onPress={this.onQuantityCombo} />
                                    </View>
                                </LinearGradient>  
                            }

                            
                        </View>
                    </TouchableOpacity>
                    )            
                }else return false;
                
            }
        )

        return (
            <View style={styles.container}>
                <View style={styles.searchWrapper}>
                    <View style={layout.searchContainer}>
                        <MaterialCommunityIcons
                            name={'magnify'}
                            size={20}
                            color={'#6b6b6b'} style={layout.iconsearchbox}
                        />
                        <TextInput
                            placeholder='Search Service' placeholderTextColor='#6b6b6b'
                            underlineColorAndroid={'transparent'}
                            style={layout.searchbox}
                            onChangeText={(searchtext) => this.changeSearchText(searchtext)}
                            ref={'searchtextinput'}
                        />

                        {this.state.showCloseSearchBox &&
                        <TouchableOpacity style={layout.iconclosesearchbox} activeOpacity={1}
                                          onPress={() => this.clearSearch()}>
                            <MaterialCommunityIcons
                                name={'close-circle-outline'}
                                size={20}
                                color={'#6b6b6b'}
                            />
                        </TouchableOpacity>
                        }
                    </View>    
                </View>
                <View style={{flex:1}}>
                    <ScrollView contentContainerStyle={styles.dataContainer} keyboardShouldPersistTaps="always">
                        {categoryDisplay}
                        {this.props.listcombo.length > 0 && isAnyCombo &&
                            <View  style={{width: width}}>
                                <View style={[styles.categoryContainer,{width: width}]}>
                                    <Text style={styles.categoryLabel}>Combos</Text>
                                </View>    
                            </View>
                        }
                        {listcombo}
                    </ScrollView>
                    {
                        this.selectServices.length > 0 && 
                        <LinearGradient
                                    start={[0, 0]}
                                    end={[1, 0]}
                                    colors={["#F069A2", "#EEAEA2"]}
                                    style={[{width:width,height:80}]}
                                >
                            <View style={[styles.selectedServices,{width:width}]}>
                                <Text style={styles.selectedServicesCount}>{this.selectServices.length} services from</Text>
                                <Text style={styles.selectedServicesPrice}> ${this.price}</Text>  
                                <View style={[styles.btnSave,{width: 220}]}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={[styles.btnLinear]}
                                        onPress={this.onSelectedServices}
                                    >
                                            <Text style={styles.btnSaveText}>Choose Time</Text>
                                    
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </LinearGradient>
                    }
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    categoryContainer:{
        height:40,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:15
    },
    categoryLabel:{
        color:'#F069A2',
        fontSize:24,
        fontFamily:'Futura'
    },
    selectedServices:{
        height:80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectedServicesCount:{
        fontSize:22,
        fontFamily:'Futura',
        color:'#fff',
        backgroundColor:'transparent'
    },
    selectedServicesPrice:{
        color:'#fff',
        fontSize:22,
        fontFamily:'Futura',
        backgroundColor:'transparent'
    },
    searchWrapper:{
        //marginBottom:15
    },
    dataContainer:{
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    serviceItem:{
        
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor:'#fff',
        flexDirection: 'row',
        marginBottom:15,
        paddingTop:20,
        paddingBottom:20,
        borderRadius:4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2

    },
    even:{
        marginLeft:15,
        marginRight:7.5,        
    },
    odd:{
        marginLeft:7.5,
        marginRight:15,      
    },
    whiteColor:{
        color:'#fff',
        backgroundColor:'transparent'
    },
    whiteDurationColor:{
        color:'rgba(255,255,255,0.6)',
        backgroundColor:'transparent'
    },
    defaultColor:{
        backgroundColor:'transparent'
    },
    serviceName:{
        fontSize:20,
        fontFamily:'Futura',
        color:'#6c6c6c'
    },
    servicePrice:{
        textAlign:'right',
        fontSize:22,
        paddingRight:20,
        color:'#F069A2',
    },
    serviceRight:{
        width:serviceRightWidth
    },
    serviceLeft:{
        paddingLeft:20
    },
    serviceDuration:{
        color:'#aeaeae',
        fontSize:16,
        marginTop:5
    },
    serviceItemSelected:{
        backgroundColor:'transparent',
    },
    containerItemSelected:{
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor:'#fff',
        flexDirection: 'row',
        marginBottom:15,
        paddingTop:20,
        paddingBottom:20,
        borderRadius:4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2
    },
    btnSave: {
        height: 60,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        marginBottom: 0,
        marginLeft:20,
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:40
    },
    btnSaveText: {
        color: "#EF75A4",
        fontSize: 30,
        zIndex: 1,
        backgroundColor: "transparent",
        fontFamily:'Futura'
        
    },
    btnSaveWraper: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:40
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden",
        flex: 1
        
    },
})