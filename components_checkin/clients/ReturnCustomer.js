import React from "react";
import {
    StyleSheet,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    TextInput,
    Keyboard,
    Alert,
    ActivityIndicator
} from "react-native";
import layout from "../../assets/styles/layout_checkin";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import { LinearGradient} from "expo";
import { formatPhone } from "../../helpers/Utils";
import emailvalidator from "email-validator";
import setting from "../../constants/Setting";

export default class ReturnCustomer extends React.Component{
    state = {
        selectedClient:0,
        showCloseSearchBox: false,
        search: '',
        data: this.props.clientSearchData
    }    

    search = '';
    searchClicked = false;
    searching = false;
    
    changeSearch = (value) => {

        value = String.prototype.trim.call(value);
        value = value.replace('(','');
        value = value.replace(')','');
        value = value.replace(' ','');
        value = value.replace('-','');
        if(value.length >= 3 && !isNaN(value)){
            let formatValue = formatPhone(value);
            this.setState({search: formatValue});
        }else{
            this.setState({search: value});
        }

        this.searchClicked = false;
        /*
        let formatValue = formatPhone(value);
        if (formatValue == "(") formatValue = "";
        this.setState({search: formatValue});
        this.search = formatValue.length == 14 ? formatValue : '';      
        if(this.search == '' && this.state.data != ''){
            this.setState({data: ''});        
        }
        this.searchClicked = false;*/
    }

    searchClient = async () => {
        let inputData = this.state.search;
        inputData = inputData.replace('(','');
        inputData = inputData.replace(')','');
        inputData = inputData.replace(' ','');
        inputData = inputData.replace('-','');
        if(String.prototype.trim.call(this.state.search) == ''){
            Alert.alert('Error','Please input Phone or Email');       
        }else if(!isNaN(inputData) && this.state.search.length != 14){
            Alert.alert('Error','Please input an valid Phone or Email');       
        }else if(isNaN(inputData) && !emailvalidator.validate(String.prototype.trim.call(this.state.search))){
            Alert.alert('Error','Please input an valid Phone or Email');      
        }else{
            if(!this.searching){
                this.searchClicked = true;
                let _this = this;
                
                /*
                let clients = this.props.clients.filter(function(item){
                    let phone = '';
                    if (typeof item.phone != 'undefined' && item.phone != '' && item.phone != null) {
                        phone = item.phone.replace(/[^\d]+/g, '');
                    }
                    return phone.indexOf(inputData) >= 0 || item.email.toLowerCase().indexOf(_this.state.search.toString().toLowerCase()) >= 0
                });*/
                Keyboard.dismiss();
                this.searching = true;
                this.setState({data: ''});
                
                await fetch(setting.apiUrl + "checkin/client/search?search="+inputData, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        Authorization: "Bearer " + this.props.token
                    }
                })
                .then(response => response.json())
                .then(responseJson => {
                    this.searching = false;
                    if (!responseJson.success) {
                        this.setState({data: ''});
                    } else {
                        this.setState({data: responseJson.data});
                    }
                })
                .catch(error => {
                    console.error(error);
                    //return [];
                });
                /*
                if(clients.length){
                    this.setState({data: clients[0]});
                }else{
                    this.setState({data: ''});
                }*/
            }
            
        }
    }

    saveClient = () => {
        this.props.onPress(this.state.data);
    }

    editClient = () => {
        this.props.onPressEdit(this.state.data);
    }

    

    render() {  
        
        let phone = '';
        let birthdate = '';
        if(this.state.data != ''){
            if (typeof this.state.data.phone != 'undefined' && this.state.data.phone != '' && this.state.data.phone != null) {
                phone = this.state.data.phone.toString().replace(/[^\d]+/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
            }  

            if (typeof this.state.data.birthdate != 'undefined' && this.state.data.birthdate != '' && this.state.data.birthdate != null) {
                //birthdate = birthdate.replace('/','');
                birthdate = this.state.data.birthdate;
            }      
        }
             
        return (
            
            <View style={styles.container}>
          
                {
                   
                    this.state.data != '' &&

                    <View>
                    <View style={[styles.clientContainer,{width: 560}]}>
                    <View style={styles.twocolumns} keyboardShouldPersistTaps='always'>
                            <View style={[styles.btnSave,{width: 270,backgroundColor:'#e8e7e7',borderRadius:5}]}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.btnSaveWraperNormal}
                                    onPress={this.editClient}
                                >
                                    <Text style={[styles.btnSaveText,{color:'#444'}]}>Edit Profile</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.btnSave,{width: 270}]}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.btnSaveWraper}
                                    onPress={this.saveClient}
                                >
                                    <LinearGradient
                                        start={[0, 0]}
                                        end={[1, 0]}
                                        colors={["#F069A2", "#EEAEA2"]}
                                        style={styles.btnLinearSave}
                                    >
                                        <Text style={styles.btnSaveText}>Next</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{backgroundColor :"#ccc",width:10} }></View>
                    <View style={[styles.clientContainer,{width: 560}]}>
                    <Text style={styles.clientTitle}>Profile Information</Text>
                        <View style={styles.clienthalfrow}>
                            <Text style={styles.clientlbl}>First Name</Text>
                            <Text style={styles.clientvalue}>{this.state.data.firstname}</Text>
                        </View>
                        <View style={styles.clienthalfrow}>
                            <Text style={styles.clientlbl}>Last Name</Text>
                            <Text style={styles.clientvalue}>{this.state.data.lastname}</Text>
                        </View>

                        <View style={styles.clienthalfrow}>
                            <Text style={styles.clientlbl}>Phone</Text>
                            <Text style={styles.clientvalue}>{phone}</Text>
                        </View>
                        <View style={styles.clienthalfrow}>
                            <Text style={styles.clientlbl}>Email</Text>
                            <Text style={styles.clientvalue}>{this.state.data.email}</Text>
                        </View>
                        
                        
                        <View style={styles.clienthalfrow}>
                            <Text style={styles.clientlbl}>Birthdate</Text>
                            <Text style={styles.clientvalue}>{birthdate}</Text>
                        </View>
                        <View style={styles.clienthalfrow}>
                            <Text style={styles.clientlbl}>Reward Point Balance</Text>
                            <Text style={styles.clientvalue}>{this.state.data.reward_point}</Text>
                        </View>
                    </View>
                    </View>
                }

                {
                    this.state.data == '' && this.state.search != '' && this.searchClicked && !this.searching && 
                    <View style={styles.clientContainer}>
                        <Text style={[styles.clientTitle,{textAlign:'center'}]}>Sorry! No result found</Text>
                    </View>
                }

                {
                    this.searching && 
                    <View style={styles.clientLoaderContainer}>
                        <View style={styles.clientLoader}>
                            <ActivityIndicator
                                color={'#F069A2'}
                                size={'large'}
                                style={styles.Indicator}
                                />
                            <View style={styles.textContainer}>
                                <Text style={[styles.textContent]}>Searching...</Text>
                            </View>
                        </View>
                    </View>
                }
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
    clientLoaderContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:20
    },
    clientLoader:{
        justifyContent: 'center',
        alignItems: 'center',
        height:100,
        width:120
    },
    Indicator:{

    },
    textContent:{
        fontSize:20,
        fontFamily:'Futura',
        marginTop:5,
        color:'#888'    
    },
    textbox:{
        height:50,
        color:'#000',
        paddingRight:20,
        paddingLeft:20,
        fontSize:16,
        backgroundColor:'#F2F2F2',
        borderBottomLeftRadius:5,
        borderTopLeftRadius:5
    },
    twocolumns:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:560
    },
    searchContainer: {     
        height:50,
        justifyContent: 'center',
        marginTop:15
    },
    searchbox:{
        position:'absolute',
        zIndex:1,
        right:0,
        width:100,
        height:50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnLinear:{
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden",
        width:100,
        height:50,
        borderBottomRightRadius:5,
        borderTopRightRadius:5
    },
    txtsearchtext:{
        backgroundColor:'transparent',
        fontSize:18,
        color:'#fff'
    },
    disable:{
        opacity: 0.5
    },
    active:{
        opacity: 1
    },
    clientContainer:{
        marginTop:15,
        justifyContent: "center",
        alignItems: "center",
    },
    clientrow:{
        flexDirection:'row',
        marginBottom:20,
        marginTop:4.5
    },
    clienthalfrow:{
        marginBottom:10,
        paddingBottom: 10,
        marginTop:4.5,
        width:460, 
        borderWidth: 0.5,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: 'rgba(249,193,152, 0.6)',
    },
    clientlbl:{
        fontSize:18,
        color:'#595c68',
        textAlign:'center',
        fontFamily:'Futura',
    },
    clientlbldot:{
        fontSize:18,
        color:'#333'
    },
    clientvalue:{
        color:'#808080',
        fontSize:18,
        marginLeft:10, 
        textAlign:"center",
        fontFamily:'Futura',
    },
    clientTitle:{
        fontSize:22,
        marginBottom:10,
        fontFamily:'Futura',
        textAlign:'center'
    },
    btnSave: {
        height: 50,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 10,
        marginBottom: 15,
        marginLeft:0
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 20,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    btnSaveWraperNormal: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5
    },
    btnLinearSave: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
    }
    
})