import React from "react";
import {
    StyleSheet,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    TextInput,
    Platform,
    Image
} from "react-native";
import layout from "../../assets/styles/layout_checkin";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import { LinearGradient} from "expo";
import ModalTechnician from "./ModalTechnician";

var width = Dimensions.get('window').width;

var columns = 2;
var columnWidthService = width / columns;
var columnWidth = width / 4;
var TechnicianWidth = (width - 75) / 4;
var serviceRightWidth = 80;
var serviceLeftWidth = serviceWidth - serviceRightWidth;
var serviceWidth = (width - 45) / columns;

export default class MultipleServiceAndCombo extends React.Component{
    state = {}
    
    search = '';
    technicianDataList = [];
    servicekey = '';
    incase = '';
    selectedServices = [];
    listServices = [];
    hour = '';
    selectedTechnician = {};
    endTime = '';
    title = '';
    servicesList = [];
    combosList = [];
    starthour = '';

    componentWillUnmount(){
        Dimensions.removeEventListener("change", () => {});
    }

    componentWillMount(){
        width = Dimensions.get('window').width;
        TechnicianWidth = (width - 75) / 4;
        columnWidth = width / 4;
        columnWidthService = width / columns;

        serviceWidth = (width - 45) / columns;
        serviceLeftWidth = serviceWidth - serviceRightWidth;

        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            //height = screen.height;
            columnWidth = width / 4;
            TechnicianWidth = (width - 75) / 4;
            columnWidthService = width / columns;

            serviceWidth = (width - 45) / columns;
            serviceLeftWidth = serviceWidth - serviceRightWidth;

            _this.setState({ appIsReady: true });
        })
    }


    onPressTechnician = (technicianSelectedModal,servicekeySelected,comboid) => {
        
        let technicianSelectedInCombo = this.selectedTechnician[comboid];
        if(Object.keys(technicianSelectedInCombo).length){    
            let techDataForAdded = {};
            techDataForAdded.fullname = technicianSelectedModal.fullname;
            techDataForAdded.id = technicianSelectedModal.id;
            techDataForAdded.start = technicianSelectedModal.start;
            techDataForAdded.end = technicianSelectedModal.end;
            techDataForAdded.duration = technicianSelectedModal.duration;
            //console.log(comboid);
            this.selectedTechnician[comboid][servicekeySelected] = techDataForAdded;         
        }
        
        //this.calculateEndHour();
        this.refs.ModalTechnician.setState({visible:false});
        this.setState({rerender: true});  

        this.props.onPress(this.selectedTechnician);    
        //this.props.onPress(technician,this.servicekey);  
        //this.setState({selectedTechnician: technician.id});      
    }

    calculateEndHour = () => {
        //loop and plus endhour by technicianId
        let arrTechEndHour = {};
        let end = 0;
        let _this = this;
        //console.log(this.selectedTechnician);
        Object.keys(this.selectedTechnician).forEach(function(itemkeySerivice){
            //console.log(itemkeySerivice);
            let ListServiceSelected = _this.selectedTechnician[itemkeySerivice];
            //console.log(ListServiceSelected);
            Object.keys(ListServiceSelected).forEach(function(itemkey){
                let item = ListServiceSelected[itemkey];
                if(typeof(arrTechEndHour[item.id]) == 'undefined'){
                    arrTechEndHour[item.id] = parseInt(item.end.replace(':',''));
                }else{
                    arrTechEndHour[item.id] = _this.getEndHour(_this.formatHourFromNumber(arrTechEndHour[item.id]),item.duration);
                }
                if(end < arrTechEndHour[item.id]){
                    end = arrTechEndHour[item.id];
                }
            })
            
        })
        this.endTime = ' - Estimated end at ' + this.formatHour(this.formatHourFromNumber(end));
        
        //console.log(this.selectedTechnician);
    }

    getTimeFromMins(mins) {
        if (mins >= 24 * 60 || mins < 0) {
            throw new RangeError("Valid input should be greater than or equal to 0 and less than 1440.");
        }
        var h = mins / 60 | 0,
            m = mins % 60 | 0;
        return (h * 100) + m;
    }

    getEndHour(startHour, duration) {
        var startInHour = startHour.split(':')[0] + "00";
        var startInMinute = startHour.split(':')[1];
        var totalMinute = parseInt(startInMinute) + parseInt(duration);
        var calculateEndHour = parseInt(startInHour) + parseInt(this.getTimeFromMins(totalMinute));
        return calculateEndHour;
    }

    formatHourFromNumber(calculateEndHour) {
        let prefix = '';
        if (calculateEndHour.toString().length == 4) {
            hour = calculateEndHour.toString().substring(0, 2);
            minute = calculateEndHour.toString().substring(2, 4);
        } else {
            hour = calculateEndHour.toString().substring(0, 1);
            minute = calculateEndHour.toString().substring(1, 3);
            prefix = '0';
        }
        return  prefix + hour + ':' + minute;
    }

    formatHour(hour) {
        var prefix = '';
        var split = hour.split(':');
        hour = parseInt(split[0]);
        minute = split[1];
        var hourformat = 'AM';
        if (parseInt(hour) >= 12) {
            if (parseInt(hour) > 12) {
                hour = parseInt(hour) - 12;
            }
            hourformat = 'PM';
        }
    
        if (hour.toString().length == 2) {
            prefix = '';
        } else {
            prefix = "0";
        }
    
        return prefix + hour + ":" + minute + " " + hourformat;
    }

    setData = (selectedServices,technicians,selectedTechnician,listServices,hour) => {
        let isService = false;
        let serviceTitle = '';
        let comboTitle = '';
        this.servicesList = [];
        this.combosList = [];
        let _this = this;
        selectedServices.forEach(function(service){
            if(service.id.indexOf('service') >= 0){
                _this.servicesList.push(service);
                serviceTitle = 'services';
            }else{
                _this.combosList.push(service);
                comboTitle = 'combos';
            }
        });
        if(serviceTitle != '' && comboTitle != ''){
            this.title = serviceTitle + ' and ' + comboTitle;
        }else  if(serviceTitle != '' && comboTitle == ''){ 
            this.title = 'services';
        }else  if(serviceTitle == '' && comboTitle != ''){ 
            this.title = 'combos';
        }

        this.technicianDataList = technicians;
        this.listServices = listServices;
        this.selectedServices = selectedServices;
        this.hour = hour;
        this.selectedTechnician = selectedTechnician;
        //this.calculateEndHour();  
        this.setState({rerender: true});    
    }

    selectTechnician = (service_name,quantity,technicians,technicianSelectedId,servicekey,comboid) => {
        //console.log(technicians);
        this.refs.ModalTechnician.setState({visible:true,servicename : service_name, quantity:quantity});
        let _this = this;
        setTimeout(function(){
            _this.refs.ModalTechnician.setData(technicians,technicianSelectedId,servicekey,comboid);
        },0)
    }

    renderItem = (service,i,key, quantityService) => {
        
        let _this = this;
        let technicianSelectedInService = this.selectedTechnician[key];
        
        let dataValidHour = this.props.onCheckValidHour(this.starthour,service, quantityService);

        let techniciansForService = dataValidHour.data[this.starthour + '_' + service.id];
        
        
        if(Object.keys(technicianSelectedInService).length && typeof(technicianSelectedInService[this.starthour + '_' + service.id]) != 'undefined'){            
            technician = technicianSelectedInService[this.starthour + '_' + service.id];
        }else{
            let isMatchHour = false;
            
            technician = techniciansForService[0];
           
            let techDataForAdded = {};
            techDataForAdded.fullname = technician.fullname;
            techDataForAdded.id = technician.id;
            techDataForAdded.start = technician.start;
            techDataForAdded.end = technician.end;
            techDataForAdded.duration = technician.duration;
            technicianSelectedInService[this.starthour + '_' + service.id] = techDataForAdded;
        }
        
        let starthourkey = this.starthour;
        let quantity = 1;
        if(typeof(service.quantity) != 'undefined'){
            quantity = service.quantity;
        }
        if(typeof(quantityService) != 'undefined'){
            quantity = quantityService;
        }
        let display = 
            <View key={key + '_' + service.id} style={{ width: columnWidthService }}>
                <View style={[styles.serviceItem, i % columns == 0 ? styles.even : styles.odd]}>
                    <View style={[styles.serviceLeft,{width: serviceLeftWidth}]}>
                        <Text style={[styles.serviceName]}>{service.service_name}</Text>
                        <View style={styles.servicetechnicianwraper}>
                            <Text style={[styles.serviceTechnicianlbl]}>Qty: </Text>
                            <Text style={[styles.serviceTechnician]}>{quantity}</Text>
                            <Text style={[styles.serviceTechnicianlbl,{marginLeft:15}]}>Technician: </Text>
                            <Text style={[styles.serviceTechnician]}>{technician.fullname}</Text>
                            <Text style={[styles.serviceTechnicianlbl,{marginLeft:15}]}>Start at: </Text>
                            <Text style={[styles.serviceTechnician]}>{this.formatHour(technician.start)}</Text>
                        </View>
                        
                    </View>
                    <View style={styles.serviceRight}>
                        <TouchableOpacity style={styles.selectTechnician} activeOpacity={1}
                                    onPress={() => {this.selectTechnician(service.service_name, quantity,techniciansForService,technician.id,starthourkey + '_' + service.id,key)}}>
                            <MaterialCommunityIcons
                                name={'chevron-down'}
                                size={30}
                                color={'#6b6b6b'}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        ;

        this.starthour = dataValidHour.hour;
        //return false;
        return display;
        /*
        let technician = {};
        //console.log(this.technicianDataList);
        let techniciansForService = this.technicianDataList[this.hour + '_' + service.id];
        if(Object.keys(technicianSelectedInService).length && typeof(technicianSelectedInService[this.hour + '_' + service.id]) != 'undefined'){            
            technician = technicianSelectedInService[this.hour + '_' + service.id];
        }else{
            let isMatchHour = false;
            
            techniciansForService.every(function(techniciansInService){
                let start = _this.formatHourFromNumber(parseInt(techniciansInService.start.replace(':','')));
                if(start == this.hour){
                    isMatchHour = true;
                    technician = techniciansInService;
                    return false;
                }
                return true;
            })
            if(!isMatchHour){
                technician = techniciansForService[0];
            }
            let techDataForAdded = {};
            techDataForAdded.fullname = technician.fullname;
            techDataForAdded.id = technician.id;
            techDataForAdded.start = technician.start;
            techDataForAdded.end = technician.end;
            techDataForAdded.duration = technician.duration;
            technicianSelectedInService[this.hour + '_' + service.id] = techDataForAdded;
        }
       
        return (
            <View key={key + '_' + service.id} style={{ width: columnWidthService }}>
                <View style={[styles.serviceItem, i % columns == 0 ? styles.even : styles.odd]}>
                    <View style={[styles.serviceLeft,{width: serviceLeftWidth}]}>
                        <Text style={[styles.serviceName]}>{service.service_name}</Text>
                        <View style={styles.servicetechnicianwraper}>
                            <Text style={[styles.serviceTechnicianlbl]}>Technician: </Text>
                            <Text style={[styles.serviceTechnician]}>{technician.fullname}</Text>
                            <Text style={[styles.serviceTechnicianlbl,{marginLeft:15}]}>Start at: </Text>
                            <Text style={[styles.serviceTechnician]}>{this.formatHour(technician.start)}</Text>
                        </View>
                        
                    </View>
                    <View style={styles.serviceRight}>
                        <TouchableOpacity style={styles.selectTechnician} activeOpacity={1}
                                    onPress={() => {this.selectTechnician(service.service_name,techniciansForService,technician.id,this.hour + '_' + service.id,key)}}>
                            <MaterialCommunityIcons
                                name={'chevron-down'}
                                size={30}
                                color={'#6b6b6b'}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );*/
    }

    renderCombo = (combo,i) => {
        let combosDisplay = [];
        let quantity = 1;
        if(typeof(combo.quantity) != 'undefined'){
            quantity = combo.quantity;
        }
        combosDisplay = combo.services.map((x, index) => {
            let serviceItemData = this.listServices.filter(function(item){
                return item.id == 'service_' + x.serviceid;        
            });
            let serviceItem = serviceItemData[0];
            //serviceItem.quantity = quantity;
            return this.renderItem(serviceItem,index,combo.id, quantity);
        })
        
        return (
            <View key={combo.id + '_' + i} style={[{width : width},styles.comboBlock]}>
                <Text style={[{width : width},styles.displayheader]}>{combo.comboname +' (x'+quantity+")"}</Text> 
                {combosDisplay}
            </View>
        )
        
    }

    showSummary = () => {
        this.props.showSummary();
    }

    render() {

        let _this = this;
        let servicesDisplay = [];
        let combosDisplay = [];
        let estimateStart = this.formatHour(this.hour);
        this.starthour = this.hour;
        if(this.selectedServices.length > 0){
            servicesDisplay = this.servicesList.map((service,i) => {
                let quantity = 1;
                if(typeof(service.quantity) != 'undefined'){
                    quantity = service.quantity;
                }
                return this.renderItem(service,i,service.id, quantity);
            })

            
            combosDisplay = this.combosList.map((combo,i) => {
                return this.renderCombo(combo,i);
            })
        }
        this.calculateEndHour();
        return (
            <View style={styles.container}>
                <View style={{flex:1}}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.mainTitle}>We recommended best matching technicians for your selected {this.title}</Text>
                    </View>
                    <Text style={styles.mainSuggestTitle}>You can change technician by clicking on icon below</Text>
                    <ScrollView contentContainerStyle={styles.dataContainer} keyboardShouldPersistTaps="always">
                        <Text style={[{width : width},styles.displayheader]}>Services</Text>
                        {servicesDisplay}
                        {combosDisplay}
                    </ScrollView>
                    <LinearGradient
                                start={[0, 0]}
                                end={[1, 0]}
                                colors={["#F069A2", "#EEAEA2"]}
                                style={[{width:width,height:80}]}
                            >
                        <View style={[styles.selectedServices,{width:width}]}>
                            {/*
                            <Text style={styles.selectedServicesCount}>Start at {estimateStart}</Text>
                            <Text style={styles.selectedServicesCount}>{this.endTime}</Text>
                            */}
                            <View style={[styles.btnSave,{width: 240}]}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={[styles.btnLinear]}
                                    onPress={this.showSummary}
                                >
                                        <Text style={styles.btnSaveText}>View Summary</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </LinearGradient>
                </View>
                <ModalTechnician visible={false} ref='ModalTechnician' onPress={this.onPressTechnician} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    titleContainer:{
        marginTop:20,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center'
    },
    mainTitle:{
        fontSize:24,
        fontFamily:'Futura',
        color:'#808080',
        paddingLeft:15,
        marginRight:15,
        textAlign:'center'
    },
    highlightTitle:{
        fontSize:24,
        fontFamily:'Futura',
        color:'#F069A2'
    },
    mainSuggestTitle:{
        fontSize:16,
        fontFamily:'Futura',
        color:'#808080',
        textAlign:'center',
        marginTop:10,
        marginBottom:20
    },
    dataContainer:{
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    comboBlock:{
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    serviceItem:{
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor:'#fff',
        flexDirection: 'row',
        marginBottom:15,
        paddingTop:20,
        paddingBottom:20,
        borderRadius:4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2

    },
    even:{
        marginLeft:15,
        marginRight:7.5,        
    },
    odd:{
        marginLeft:7.5,
        marginRight:15,      
    },
    serviceRight:{
        width:serviceRightWidth,
        alignItems: 'flex-end',
        paddingRight:20
    },
    serviceLeft:{
        paddingLeft:20
    },
    serviceName:{
        fontSize:20,
        fontFamily:'Futura',
        color:'#808080'
    },
    serviceTechnician:{
        color:'#F069A2',
        fontSize:16,
        marginTop:5
    },
    serviceTechnicianlbl:{
        color:'#aeaeae',
        fontSize:16,
        marginTop:5
    },
    servicetechnicianwraper:{
        flexDirection: 'row',
    },
    selectTechnician:{

    },
    selectedServices:{
        height:80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectedServicesCount:{
        fontSize:22,
        fontFamily:'Futura',
        color:'#fff',
        backgroundColor:'transparent'
    },
    btnSave: {
        height: 60,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        marginBottom: 0,
        marginLeft:20,
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:40
    },
    btnSaveText: {
        color: "#EF75A4",
        fontSize: 30,
        zIndex: 1,
        backgroundColor: "transparent",
        fontFamily:'Futura'
        
    },
    btnSaveWraper: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:40
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden",
        flex: 1
        
    },
    displayheader:{
        fontSize:24,
        fontFamily:'Futura',
        color:'#F069A2',
        justifyContent: "center",
        alignItems: "center",
        textAlign:'center',
        marginTop:0,
        marginBottom:10
    }
})