import React from "react";
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    ActivityIndicator,
    TouchableOpacity,
    AlertIOS,
    Alert,
    Image,
    Modal,
    Platform 
} from "react-native";
import { LinearGradient, Svg } from "expo";

import layout from "../assets/styles/layout_checkin";
import Colors from "../constants/Colors_checkin";

import setting from "../constants/Setting";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import ImageResponsive from 'react-native-scalable-image';

//import layout from "../assets/styles/layout";
var {height, width} = Dimensions.get('window');
var marginTopByScreen = 0;
var marginBottomTextByScreen = 0;
var marginBottomButtonTextByScreen = 0;
var paddingBottomLogo = 0;
if(Platform.OS != "ios"){
    paddingBottomLogo = 70;
}
if(height > 1020){
    marginTopByScreen = 40;
    marginBottomTextByScreen = 30;
    marginBottomButtonTextByScreen = 70;
}else{
    marginTopByScreen = 0;
    marginBottomTextByScreen = 0;
    marginBottomButtonTextByScreen = 30;
}
//portrait default
var stepcontainerwidth = 750;
var stepwidth = 230;
var seperatorwidth = 30;
var stepfontsize = 16;
var stephintsize = 14;

if(width > height){
    stepcontainerwidth = 960;
    stepwidth = 300;
    seperatorwidth = 30;
    stepfontsize = 22;
    stephintsize = 17;
}

export default class ModalHowitwork extends React.Component {
    state = {
        modalVisible: false,
        styleAnimation: 'slide'
    }

    componentWillUnmount(){
        Dimensions.removeEventListener("change", () => {});
    }

    async componentWillMount() {
        
        var screen = Dimensions.get('window');
        width = screen.width;
        height = screen.height;
        
        var marginTopByScreen = 0;
        var marginBottomTextByScreen = 0;
        var marginBottomButtonTextByScreen = 0;
        if(height > 1020){
            marginTopByScreen = 40;
            marginBottomTextByScreen = 30;
            marginBottomButtonTextByScreen = 70;
        }else{
            marginTopByScreen = 0;
            marginBottomTextByScreen = 0;
            marginBottomButtonTextByScreen = 30;
        }

        if(width > height){
            stepcontainerwidth = 960;
            stepwidth = 300;
            seperatorwidth = 30;
            stepfontsize = 22;
            stephintsize = 17;
        }

        let _this = this;
        Dimensions.addEventListener('change',function(){
            var screen = Dimensions.get('window');
            width = screen.width;
            height = screen.height;

            if(width > height){
                stepcontainerwidth = 960;
                stepwidth = 300;
                seperatorwidth = 30;
                stepfontsize = 22;
                stephintsize = 17;
            }else{
                stepcontainerwidth = 750;
                stepwidth = 230;
                seperatorwidth = 30;
                stepfontsize = 16;
                stephintsize = 14;
            }    

            var marginTopByScreen = 0;
            var marginBottomTextByScreen = 0;
            var marginBottomButtonTextByScreen = 0;
            if(height > 1020){
                marginTopByScreen = 40;
                marginBottomTextByScreen = 30;
                marginBottomButtonTextByScreen = 70;
            }else{
                marginTopByScreen = 0;
                marginBottomTextByScreen = 0;
                marginBottomButtonTextByScreen = 30;
            }
            _this.setState({ appIsReady: true });
        })
    }

    close = () => {
        
        this.setState({modalVisible: false});
    }

    show = () => {
        this.setState({styleAnimation : 'slide',modalVisible: true });
    }
    
    start = () => {
        this.setState({styleAnimation : 'none'});
        let _this = this;
        setTimeout(() => {
            _this.close();
            _this.props.onStart();
        },1)
    }

    render() {
        return(
            <Modal
                animationType={this.state.styleAnimation}
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}

            >
                <View style={styles.container}>
                    <View style={[styles.maincontainer]}>
                            <TouchableOpacity style={styles.closebtnright} activeOpacity={1}
                                onPress={() => this.close()}>
                                <MaterialCommunityIcons
                                    name={'close'}
                                    size={30}
                                    color={'rgba(255,255,255,1)'} style={styles.navIconIOS}
                                />
                            </TouchableOpacity>
                            <View style={[styles.header,{width:width}]}>
                                <LinearGradient start={[0, 0]} end={[1, 0]} colors={["#F069A2", "#EECBA3"]} style={{position:'relative',zIndex:1}}>
                                    <View style={{width:width,alignItems: 'center',justifyContent: 'center',position:'relative',top:50,zIndex:1,paddingBottom:paddingBottomLogo}}>
                                        <Text style={[styles.logofontdefault,styles.logofont]}>How It Works</Text>
                                      
                                    </View>     
                                </LinearGradient>
                                {
                                    Platform.OS === "ios" && 
                                    <Svg height={100} width={width}>
                                        <Svg.Defs>
                                            <Svg.LinearGradient id="grad" x1="0%" y1="0%" x2="100%" y2="0%">
                                                <Svg.Stop offset="0%" stopColor="#F069A2" stopOpacity="1" />
                                                <Svg.Stop offset="100%" stopColor="#EECBA3" stopOpacity="1" />
                                            </Svg.LinearGradient>
                                            
                                        </Svg.Defs>
                            
                                        <Svg.Ellipse fill="url(#grad)" cx={width / 2} cy="0" rx={width / 2 + 100} ry="100"></Svg.Ellipse>
                                    </Svg>
                                }
                                
                            </View>
                                
                            
                            <View style={{flex:1,width:width,alignItems:'center',marginTop:marginTopByScreen}}>
                                <Text style={[styles.hometitle,{marginBottom:marginBottomTextByScreen}]}>We provide an easy and efficient way of booking nail services</Text>   
                                <View style={[styles.benefitcontainer,{width:stepcontainerwidth}]}>
                                    <View style={[styles.iconitem,{width:stepwidth}]}>
                                        <Text style={[styles.iconstep,{fontSize:stepfontsize}]}>STEP 1</Text>
                                        <Text style={[styles.iconlbl,{fontSize:stepfontsize}]}>Enter Phone or Email</Text>
                                    </View>
                                    <View style={[styles.iconseperator,{width:seperatorwidth}]}>
                                        <ImageResponsive 
                                            source={require('../assets/icons/hand-01.png')}
                                            height={15}
                                        />
                                    </View>
                                    <View style={[styles.iconitem,{width:stepwidth}]}>
                                        <Text style={[styles.iconstep,{fontSize:stepfontsize}]}>STEP 2</Text>
                                        <Text style={[styles.iconlbl,{fontSize:stepfontsize}]}>Continues if existing customer</Text>
                                        <Text style={[styles.iconhint,{fontSize: stephintsize}]}>If not please sign up</Text>
                                    </View>
                                    <View style={[styles.iconseperator,{width:seperatorwidth}]}>
                                        <ImageResponsive 
                                            source={require('../assets/icons/hand-01.png')}
                                            height={15}
                                        />
                                    </View>
                                    <View style={[styles.iconitem,{width:stepwidth}]}>
                                        <Text style={[styles.iconstep,{fontSize:stepfontsize}]}>STEP 3</Text>
                                        <Text style={[styles.iconlbl,{fontSize:stepfontsize}]}>Choose "Service"</Text>
                                        <Text style={[styles.iconhint,{fontSize: stephintsize}]}>(To search your desired services)</Text>
                                    </View>
                                </View>
                                <View style={[styles.benefitcontainernobg,{width:stepcontainerwidth}]}>
                                    <View style={[styles.iconitemnobg,{width: stepwidth}]}></View>
                                    <View style={[styles.iconseperator,{width:seperatorwidth}]}></View>
                                    <View style={[styles.iconitemnobg,{width: stepwidth}]}></View>
                                    <View style={[styles.iconseperator,{width:seperatorwidth}]}></View>
                                    <View style={[styles.iconitemnobg,{width: stepwidth}]}>
                                        <ImageResponsive 
                                            source={require('../assets/icons/hand-03.png')}
                                            height={20}
                                        />
                                    </View>
                                </View>
                                <View style={[styles.benefitcontainernobg,{width:stepcontainerwidth}]}>
                                    <View style={[styles.iconitem,{width:stepwidth}]}>
                                        <Text style={[styles.iconstep,{fontSize:stepfontsize}]}>STEP 6</Text>
                                        <Text style={[styles.iconlbl,{fontSize:stepfontsize}]}>Confirm & Book</Text>
                                        <Text style={[styles.iconhint,{fontSize: stephintsize}]}>(Complete)</Text>
                                    </View>
                                    
                                    <View style={[styles.iconseperator,{width:seperatorwidth}]}>
                                        <ImageResponsive 
                                            source={require('../assets/icons/hand-02.png')}
                                            height={15}
                                        />
                                    </View>
                                    <View style={[styles.iconitem,{width:stepwidth}]}>
                                        <Text style={[styles.iconstep,{fontSize:stepfontsize}]}>STEP 5</Text>
                                        <Text style={[styles.iconlbl,{fontSize:stepfontsize}]}>Choose "Technician"</Text>
                                        <Text style={[styles.iconhint,{fontSize: stephintsize}]}>(Click "Any Technician" to select Technician for each service)</Text>
                                    </View>
                                    <View style={styles.iconseperator}>
                                        <ImageResponsive 
                                            source={require('../assets/icons/hand-02.png')}
                                            height={15}
                                        />
                                    </View>
                                    <View style={[styles.iconitem,{width:stepwidth}]}>
                                        <Text style={[styles.iconstep,{fontSize:stepfontsize}]}>STEP 4</Text>
                                        <Text style={[styles.iconlbl,{fontSize:stepfontsize}]}>Choose "Time"</Text>
                                    </View>
                                </View>
                                <TouchableOpacity  activeOpacity={1} style={[styles.btnHome,{marginTop:marginBottomButtonTextByScreen}]} onPress={this.start}>  
                                    <LinearGradient
                                        start={[0, 0]}
                                        end={[1, 0]}
                                        colors={["#72BC3F", "#72BC3F"]}
                                        style={styles.btnLinear}
                                    >
                                        <Text style={styles.btnHomeText}>Start Your Appointment Now</Text>
                                    </LinearGradient>
                                    
                                </TouchableOpacity>
                            </View>
                        </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    header:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        backgroundColor:'#fff'
    },
    welcome:{
        fontSize:24,
        backgroundColor:'transparent',
        color:'#fff',
        fontFamily: 'futuralight',
        marginBottom:10
    },
    copyright:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#fff',
        fontFamily: 'Futura',
        position:'absolute',
        bottom:40   
    },
    copyrightPhone:{
        fontSize:18,
        backgroundColor:'transparent',
        color:'#fff',
        fontFamily: 'Futura',
        position:'absolute',
        bottom:10   
    },
    maincontainer: {
        flex: 1,
        /*justifyContent: 'center',*/
        alignItems: 'center',
    },
    maincontainerLoad: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentscontainer:{
        backgroundColor:'rgba(255,255,255,0.2)',
        paddingTop:20,
        paddingBottom:20,
        paddingLeft:20,
        paddingRight:20,
        borderRadius:10,
        alignItems: 'center',
    },
    containerGradient: {
        flex: 1,
		opacity:0.8,
		position:'absolute',
		top:0,
		bottom:0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:1
    },
    loadingText:{
        color:'#fff',
        fontSize:20,
    },
    loadingText1:{
        color:'#fff',
        fontSize:22,
        marginTop:-5
    },
    underline:{
        width:100,
        height:1,
        backgroundColor:'#fff',
        marginTop:10,
        marginBottom:10
    },
    backgroundFullscreen:{
        position:'absolute',
    },
    logofontdefault:{
        backgroundColor:'transparent',
        color:'#fff',
        fontSize:60,
        marginBottom:0,
        textAlign:'center',
        paddingLeft:20,
        paddingRight:20
    },
    logofont:{
        fontFamily: 'futuralight'
    },
 
    btnHome:{
        //backgroundColor:'#EF75A4',
        marginBottom:40,
        alignItems:'center',
        justifyContent:'center'
    },
    iconmain:{
        marginRight:15,
        marginTop:4
    },
    logo:{
        marginBottom:70
    },
    
    logo2:{
        marginBottom:0
    },
    hometitle:{
        fontFamily:'futuralight',
        textAlign:'center',
        fontSize:32,
        marginTop:30
    },
    benefitcontainer:{
        flexDirection:'row',
        marginTop:40
    },
    iconstep:{
        fontFamily:'Futura',
        textAlign:'center',
        marginBottom:5
    },
    iconlbl:{
        fontFamily:'futuralight',
        textAlign:'center',
        fontSize:18
    },
    iconseperator:{
        width:30,
        alignItems:'center',
        justifyContent:'center'
    },
    iconitem:{
        backgroundColor:'#FFE9E7',
        justifyContent: "center",
        alignItems: "center",        
        padding:10
    },
    iconitemnobg:{
        justifyContent: "center",
        alignItems: "center",
    },
    benefitcontainernobg:{
     
        flexDirection:'row',
        marginTop:10
    },
    iconhint:{
        fontFamily:'futuralight',
        textAlign:'center',
        color:'#cc5967',
        marginTop:5
    },
    btncontainer:{
        width:600,
        flexDirection:'row',
        marginTop:40,
        alignItems:'center',
        justifyContent:'center'
    },
    
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
   
        borderRadius:40,
        padding:5,
        height:50,
        width:300,
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 1,
    },
    closebtnright:{
        position:'absolute',
        left:20,
        backgroundColor:'transparent',
        top:35,
        zIndex:100
    },
    btnHome:{
        alignItems: 'center',
        marginBottom:40,
        alignItems:'center',
        justifyContent:'center',
        marginTop:40
    },
    btnHomeText:{
        color:'#fff',
        fontSize:24,
        fontFamily:'Futura',
        textAlign:'center',
        backgroundColor:'transparent'
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
   
        borderRadius:40,
        padding:5,
        height:50,
        width:350,
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 1,
    }
});
