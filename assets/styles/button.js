import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    login: {
        backgroundColor:'#fff',
        height:55,
        borderRadius:40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginsocial: {
        height:38,
        borderRadius:20,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor:'#fff',
        borderWidth:1,
    },
    txtLogin: {
        color:'#EF75A4',
        fontSize:20
    },
    txtLoginSocial: {
        color:'#fff'
    },
    txtSignUp: {
        color:'#fff',
        fontSize:12
    }
});
