/**
 * @flow
 */
import React from "react";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {TabNavigator, TabBarBottom} from "react-navigation";
import Colors from "../constants/Colors";
import CalendarScreen from "../screens/CalendarScreen";
import LinksScreen from "../screens/LinksScreen";
import SettingsScreen from "../screens/SettingsScreen";
import {StyleSheet, View, Text} from "react-native";

export default TabNavigator(
    {
        Calendar: {
            screen: CalendarScreen,
        },
        Dashboard: {
            screen: LinksScreen,
        },
        More: {
            screen: SettingsScreen,
        },
    },
    {
        navigationOptions: ({navigation}) => ({
            // Set the tab bar icon
            tabBarIcon: ({focused}) => {
                const {routeName} = navigation.state;
                let iconName, tabName;
                switch (routeName) {
                    case 'Calendar':
                        iconName = 'calendar-text';
                        tabName = 'CALENDAR';
                        break;
                    case 'Dashboard':
                        iconName = 'home-outline';
                        tabName = 'DASHBOARD';
                        break;
                    case 'More':
                        iconName = 'dots-horizontal';
                        tabName = 'MORE';
                }
                return (
                    <MaterialCommunityIcons
                        name={iconName}
                        label={tabName}
                        size={32}
                        color={focused ? Colors.tabIconSelected : Colors.tabIconDefault} style={styles.tabCenter}
                    />
                );
            },
        }),
        // Put tab bar on bottom of screen on both platforms
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        // Disable animation so that iOS/Android have same behaviors
        animationEnabled: false,
        // Don't show the labels
        tabBarOptions: {
            showLabel: true,
        },
    }
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tabCenter: {
        textAlign: "center"
    }
});
