//  Created by react-native-create-bridge

import { NativeModules , NativeEventEmitter, Alert, StyleSheet, Platform, View} from 'react-native'
import React from "react";
import Settings from './constants/Setting';
import layout from "./assets/styles/layout";
import FloatLabelTextField from "./components/FloatTextInput";
import SubmitLoader from "./helpers/submitloader";
import Colors from "./constants/Colors";
import SpinnerLoader from "./helpers/spinner";
const { CardConnect } = NativeModules
let isconnect = false;
const CardConnectEmitter = new NativeEventEmitter(CardConnect);
 const connectionStateHasChanged = CardConnectEmitter.addListener(
  'connectionStateHasChanged',
  (data) => {
      console.log(data.state);
  }
); 




export default class CardConnectHelper extends React.Component{

  amount = this.props.amount;
  ccnumber = "";
  ccholdername = "";
  ccexpiredate = "";
  accountType = "";
  componentWillMount(){
    CardConnect.connect(Settings.cardconnectEndPoint);
    if(!isconnect){
     //CardConnect.connect(Settings.cardconnectEndPoint);
    }else{
      this.props.connected("Connected");
    }
    CardConnectEmitter.addListener(
      'connectionStateHasChanged',
      (data) => {
          console.log(data.state);
          this.props.checkConnect(data.state);
           switch (data.state){
            case "Connected":
                isconnect = true;
              break;
              default:
              isconnect = false;
          }

      }
    );
    CardConnectEmitter.addListener(
      'swipeStarted',
      () => {
        this.props.swipeStarte("started");
          //console.log("started");
      }
    );
    CardConnectEmitter.addListener(
      'swipeSuccess',
      (data) => {
        console.log(data);
        this.props.swipeSuccess(data);
      }
    );

    CardConnectEmitter.addListener(
      'swipeError',
      (data) => {
        this.props.swipeStarte(data.message);
          //console.log(data.message);
      }
    );
  }
  /*
  connect: (callback) => {
    return CardConnect.connect(callback)
  }*/

  /*
  connect: () => {
    return CardConnect.connect();
  },

  disconnect: () => {

  }*/
  get=()=>{
    return connectionStateHasChanged;
  }
  connect = () => {

    return CardConnect.connect(Settings.cardconnectEndPoint);
  }
  onChangeTextCCHoldername = value => {
    this.refs.ccholdernameinput.setState({ text: value });
    this.ccholdername = value;
};
  disconnect = () => {
    connectionStateHasChanged.remove();
    swipeError.remove();
    swipeStarted.remove();
    swipeSuccess.remove();
    
  }

  render(){
    return false;
  }
  
  // use this for async await
  /*
  exampleMethod () {
    return CardConnect.exampleMethod()
  },*/

  

  //EXAMPLE_CONSTANT: CardConnect.EXAMPLE
}


/*const CardConnectHelper = {
  show: () => {
    <SubmitLoader
        ref="appointmentLoader"
        visible={false}
        textStyle={layout.textLoaderScreenSubmit}
        textContent={this.getText('processing')}
        color={Colors.spinnerLoaderColorSubmit}
    />
  }
}*/
