import React from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    Platform,
    TextInput,
    FlatList
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo";
import layout from "../assets/styles/layout";
import { TextInputMask } from "react-native-masked-text";
import { getTextByKey } from "../helpers/language";
import FloatLabelTextField from "../components/FloatTextInput";
import {
    inputCurrency
} from "../helpers/Utils";

export default class PaymentCash extends React.Component {
    state = {
        modalVisible: this.props.visible,
        value: this.props.value,
        remaining: this.props.remaining
    };
    amount = 0;

    close() {
        this.setState({ modalVisible: false });
    }

    show = () => {
        this.setState({ modalVisible: true });
    };

    onChangeText = value => {
        /*
        if (value != "") {
            value = value.replace("$", "");
            value = value.replace(",", "");
        }*/

        //this.amount = value;
        value = inputCurrency(value);
        this.amount = value;
        this.refs.ccamountinput.setState({ text: value });
    };

    addAmount = () => {
        //console.log(this.amount);
        this.props.onSave(this.amount);
    };

    onDelete = () => {
        this.props.onSave("");
        //console.log(this.amount);
    };

    getText(key){
        return getTextByKey(this.props.language,key);
    }

    render() {
        //console.log(this.props.data);
        let calculateAmount = this.state.value;
        if(calculateAmount == ""){
            calculateAmount = this.state.remaining;
        }
        this.amount = "$" + calculateAmount;
        //calculateAmount = calculateAmount.toFixed(2);
        //console.log(calculateAmount);
        return (
            <Modal
                animationType={"none"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}
            >
                <View
                    style={
                        Platform.OS === "android"
                            ? layout.headercontainerAndroid
                            : layout.headercontainer
                    }
                >
                    <LinearGradient
                        start={[0, 0]}
                        end={[1, 0]}
                        colors={["#F069A2", "#EEAEA2"]}
                        style={
                            Platform.OS === "android"
                                ? layout.headerAndroid
                                : layout.header
                        }
                    >
                        <View style={layout.headercontrols}>
                            <TouchableOpacity
                                style={layout.headerNavLeftContainer}
                                activeOpacity={1}
                                onPress={() => this.close()}
                            >
                                <View style={layout.headerNavLeft}>
                                    <MaterialCommunityIcons
                                        name={"chevron-left"}
                                        size={30}
                                        color={"rgba(255,255,255,1)"}
                                        style={
                                            Platform.OS === "android"
                                                ? layout.navIcon
                                                : layout.navIconIOS
                                        }
                                    />
                                </View>
                            </TouchableOpacity>
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}
                            >
                                <Text style={layout.headertitle}>{this.getText('paymentcash')}</Text>
                            </View>
                            {this.amount != "" &&
                                <TouchableOpacity
                                    style={layout.headerNavRightContainer}
                                    activeOpacity={1}
                                    onPress={() => this.onDelete()}
                                >
                                    <View style={layout.headerNavRight}>
                                        <Text style={layout.headerNavText}>
                                            {this.getText('delete')}
                                        </Text>
                                    </View>
                                </TouchableOpacity>}
                        </View>
                    </LinearGradient>
                </View>

                <View style={{ flex: 1, backgroundColor: "#F2F2F2" }}>
                                {/*
                    <TextInputMask
                        style={styles.inputamount}
                        placeholder="$0.00"
                        type={"money"}
                        underlineColorAndroid="transparent"
                        onChangeText={this.onChangeText.bind(this)}
                        value={calculateAmount}
                        options={{
                            precision: 2,
                            separator: ".",
                            delimiter: ",",
                            unit: "$"
                        }}
                    />*/}
                    <View style={layout.floatGroup}>
                        <FloatLabelTextField
                            placeholder={"Amount"}
                            value={this.amount}
                            onChangeTextValue={this.onChangeText.bind(
                                this
                            )}
                            underlineColorAndroid="transparent"
                            ref="ccamountinput"
                        />
                    </View>
                    <View style={styles.btnSave}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.btnSaveWraper}
                            onPress={this.addAmount}
                        >
                            <LinearGradient
                                start={[0, 0]}
                                end={[1, 0]}
                                colors={["#F069A2", "#EEAEA2"]}
                                style={styles.btnLinear}
                            >
                                <Text style={styles.btnSaveText}>{this.getText('paymentadd')}</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    inputamount: {
        height: 45,
        justifyContent: "center",
        alignItems: "center",
        paddingRight: 15,
        paddingLeft: 15,
        backgroundColor: "#fff",
        borderWidth: 1,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderColor: "#ddd"
    },
    btnSave: {
        height: 45,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 16,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 15,
        right: 15
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
    }
});
