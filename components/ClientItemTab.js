import React from "react";
import { StyleSheet ,Text, View, TouchableOpacity, Dimensions } from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {
    getUserData
} from "../helpers/authenticate";
var {height, width} = Dimensions.get('window');
export default class ClientItemTab extends React.PureComponent {
    async componentWillMount() {
        var screen = Dimensions.get('window');
        width = screen.width;
        height = screen.height;
        this.userData = await getUserData();
    }
    _onPress = () => {
        if((this.userData.view_customer_information != "" && this.userData.view_customer_information == 1)
        || this.userData.role == 4){
            this.props.onPressItem(this.props.id,this.props.name);
        }
        
    };

    render() {
        return(
            <TouchableOpacity activeOpacity={1} onPress={() => this._onPress()}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={[styles.itemContainer,{width: width - 350}]} >
                        <Text style={styles.technicianname}>{this.props.name}</Text>
                    </View>
                    <View style={[styles.itemContainer,{width: 200}]} >
                        <Text style={styles.technicianname}>{this.props.data.lastvisit}</Text>
                    </View>
                    <View style={[styles.itemContainer,{width: 150}]} >
                        <Text style={styles.technicianname}>{this.props.data.reward_point} Points</Text>
                    </View>
                </View>
        </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        height:50,
        borderBottomColor: '#CFD4DA',
        borderBottomWidth: 0.5,
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:'#fff',
        justifyContent:'center',
    },
    technicianname:{
        marginLeft:5,
        color:'#6b6b6b',
        fontSize:16
    }
});
