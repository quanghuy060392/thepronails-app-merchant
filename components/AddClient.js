import React from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    Platform,
    TextInput,
    ScrollView,
    Dimensions,
    Alert
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo";
import layout from "../assets/styles/layout";
import FloatLabelTextField from "../components/FloatTextInput";
import { TextInputMask } from "react-native-masked-text";
import { formatPhone, formatMonth } from "../helpers/Utils";
import emailvalidator from "email-validator";
import Colors from "../constants/Colors";
import SubmitLoader from "../helpers/submitloader";
import IconLoader from "../helpers/iconloader";
import setting from "../constants/Setting";
import { StackNavigation } from "@expo/ex-navigation";
import { getTextByKey } from "../helpers/language";

export default class AddClient extends React.Component {
    state = {
        modalVisible: this.props.visible
    };

    title = "";
    clientData = {};
    columnWidth = Dimensions.get("window").width / 2;

    resetData = () => {
        this.clientData.id = 0;
        this.clientData.firstname = "";
        this.clientData.lastname = "";
        this.clientData.email = "";
        this.clientData.phone = "";
        this.clientData.month = "";
        this.clientData.day = "";
        this.clientData.rewardpoint = "";
    };

    close() {
        //this.props.onClose();
        this.setState({ modalVisible: false });
    }

    show = () => {
        this.setState({ modalVisible: true });
    };

    onChangeTextFirstName = value => {
        this.refs.txtfirstnameinput.setState({ text: value });
        this.clientData.firstname = value;
    };

    onChangeTextLastName = value => {
        this.refs.txtlastnameinput.setState({ text: value });
        this.clientData.lastname = value;
    };

    onChangeTextEmail = value => {
        this.refs.txtemailinput.setState({ text: value });
        this.clientData.email = value;
    };

    onChangeTextPhone = value => {
        let formatValue = formatPhone(value);
        if (formatValue == "(") formatValue = "";
        this.refs.txtphoneinput.setState({ text: formatValue });
        this.clientData.phone = formatValue;
    };
    onChangeTextreward = value => {
        this.refs.txtrewardinput.setState({ text: value });
        this.clientData.rewardpoint = value;
    };
    onChangeTextMonth = value => {
        let formatValue = formatMonth(value);
        if (formatValue != "" && parseInt(formatValue) > 12) formatValue = "12";
        this.refs.txtmonthinput.setState({ text: formatValue });
        this.clientData.month = formatValue;
    };

    onChangeTextDay = value => {
        let formatValue = formatMonth(value);
        if (formatValue != "" && parseInt(formatValue) > 31) formatValue = "31";
        this.refs.txtdayinput.setState({ text: formatValue });
        this.clientData.day = formatValue;
    };

    saveClient = () => {
        let isValid = true;
         
       /* if (String.prototype.trim.call(this.clientData.firstname) == "") {
            isValid = false;
            Alert.alert("Error", this.getText('addclientfirstnamerequire'));
        } else if (String.prototype.trim.call(this.clientData.lastname) == "") {
            isValid = false;
            Alert.alert("Error", this.getText('addclientlastnamerequire'));
        } */
       /* else if (String.prototype.trim.call(this.clientData.email) == "") {
            isValid = false;
            Alert.alert("Error", this.getText('addclientemailrequire'));
        } */
        if (String.prototype.trim.call(this.clientData.email) != "") {
            if(!emailvalidator.validate(String.prototype.trim.call(this.clientData.email))){
                isValid = false;
                Alert.alert("Error", this.getText('addclientemailvalidrequire'));
            }
        } else if (
            String.prototype.trim.call(this.clientData.phone) != "" &&
            this.clientData.phone.length != 14
        ) {
            isValid = false;
            Alert.alert(
                "Error",
                this.getText('addclientphonevalidrequire')
            );
        } else if (
            String.prototype.trim.call(this.clientData.month) != "" ||
            String.prototype.trim.call(this.clientData.day) != ""
        ) {
            if (String.prototype.trim.call(this.clientData.month) == "") {
                isValid = false;
                Alert.alert("Error", this.getText('addclientmonthrequire'));
            } else if (String.prototype.trim.call(this.clientData.day) == "") {
                isValid = false;
                Alert.alert("Error", this.getText('addclientdayrequire'));
            }
        }else if(String.prototype.trim.call(this.clientData.email) == "" && String.prototype.trim.call(this.clientData.phone) == ""){
            isValid = false;
            Alert.alert("Error", this.getText('addclientemailorphonerequire'));
        }

        if (isValid) {
            var formdata = {};
            formdata.id = this.clientData.id;
            formdata.rewardpoint = this.clientData.rewardpoint;
            formdata.firstname = String.prototype.trim.call(
                this.clientData.firstname
            );
            formdata.lastname = String.prototype.trim.call(
                this.clientData.lastname
            );
            formdata.email = String.prototype.trim.call(this.clientData.email);

            if (
                String.prototype.trim.call(this.clientData.phone) != "" &&
                this.clientData.phone.length == 14
            ) {
                formdata.phone = this.clientData.phone;
            }
            if (
                String.prototype.trim.call(this.clientData.month) != "" &&
                String.prototype.trim.call(this.clientData.day) != ""
            ) {
                formdata.birthdate =
                    this.clientData.month + "/" + this.clientData.day;
            }
            this.refs.clientLoader.setState({ visible: true });   
            fetch(setting.apiUrl + "client/update", {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.props.token
                },
                body: JSON.stringify(formdata)
            })
                .then(response => response.json())
                .then(responseJson => {
                    if (!responseJson.success) {
                        //console.log(responseJson);
                        this.refs.clientLoader.setState({ visible: false });   
                        let _this = this;
                        setTimeout(function() {
                            _this.fetchError(responseJson);
                        }, 100);

                        //Alert.alert('Error', responseJson.message);
                        //return [];
                    } else {
                        this.refs.clientLoader.setState({ visible: false });   

                        let successMessage = this.getText('clientsavedlbl');
                        if (formdata.id > 0) {
                            successMessage = this.getText('clientupdatedlbl');
                        }
                        this.refs.clientSuccessLoader.setState({
                            textContent: successMessage,
                            visible: true
                        });

                        let _this = this;
                        setTimeout(function() {
                            _this.refs.clientSuccessLoader.setState({
                                visible: false
                            });
                            _this.props.SaveClientSuccess(
                                _this.clientData.id,
                                responseJson.data
                            );
                        }, 2000);

                        //Alert.alert("OK", "Appointment Booked");
                    }
                })
                .catch(error => {  
                    console.error(error);
                    //return [];
                });
        }
    };

    fetchError(responseJson) {
        if (
            responseJson.message == "token_expired" ||
            responseJson.message == "token_invalid"
        ) {
            
            let rootNavigator = this.props.navigation.getNavigator('root'); 
            rootNavigator.replace('login');
        } else {
            Alert.alert("Error", responseJson.message);
        }
    }

    getText(key){
        return getTextByKey(this.props.language,key);
    }

    render() {
        //console.log(this.props.data);

        return (
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}
            >
                <View
                    style={
                        Platform.OS === "android"
                            ? layout.headercontainerAndroid
                            : layout.headercontainer
                    }
                >
                    <LinearGradient
                        start={[0, 0]}
                        end={[1, 0]}
                        colors={["#F069A2", "#EEAEA2"]}
                        style={
                            Platform.OS === "android"
                                ? layout.headerAndroid
                                : layout.header
                        }
                    >
                        <View style={layout.headercontrols}>
                            <TouchableOpacity
                                style={layout.headerNavLeftContainer}
                                activeOpacity={1}
                                onPress={() => this.close()}
                            >
                                <View style={layout.headerNavLeft}>
                                    <MaterialCommunityIcons
                                        name={"close"}
                                        size={30}
                                        color={"rgba(255,255,255,1)"}
                                        style={
                                            Platform.OS === "android"
                                                ? layout.navIcon
                                                : layout.navIconIOS
                                        }
                                    />
                                </View>
                            </TouchableOpacity>
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}
                            >
                                <Text style={layout.headertitle}>
                                    {this.getText(this.title)}
                                </Text>
                            </View>
                        </View>
                    </LinearGradient>
                </View>

                <ScrollView style={styles.container} keyboardShouldPersistTaps='always'>
                    <View style={layout.floatGroup}>
                        <FloatLabelTextField
                            placeholder={this.getText('clientfirstname')}
                            value={this.clientData.firstname}
                            onChangeTextValue={this.onChangeTextFirstName}
                            underlineColorAndroid="transparent"
                            ref="txtfirstnameinput"
                        />
                    </View>
                    <View style={layout.floatGroup}>
                        <FloatLabelTextField
                            placeholder={this.getText('clientlastname')}
                            value={this.clientData.lastname}
                            onChangeTextValue={this.onChangeTextLastName}
                            underlineColorAndroid="transparent"
                            ref="txtlastnameinput"
                        />
                    </View>
                    <View style={layout.floatGroup}>
                        <FloatLabelTextField
                            placeholder={"Email"}
                            value={this.clientData.email}
                            onChangeTextValue={this.onChangeTextEmail}
                            underlineColorAndroid="transparent"
                            ref="txtemailinput"
                        />
                    </View>

                    <View style={layout.floatGroup}>
                        <FloatLabelTextField
                            placeholder={this.getText('clientphonelbl')}
                            value={this.clientData.phone}
                            onChangeTextValue={this.onChangeTextPhone}
                            underlineColorAndroid="transparent"
                            ref="txtphoneinput"
                        />
                    </View>
                    <View style={[layout.floatGroup, styles.twocolumn]}>
                        <FloatLabelTextField
                            placeholder={this.getText('clientmonthlbl')}
                            value={this.clientData.month}
                            onChangeTextValue={this.onChangeTextMonth}
                            underlineColorAndroid="transparent"
                            ref="txtmonthinput"
                            style={[{ width: this.columnWidth - 1 }]}
                        />
                        <View style={styles.seperatecolumn} />
                        <FloatLabelTextField
                            placeholder={this.getText('clientdaylbl')}
                            value={this.clientData.day}
                            onChangeTextValue={this.onChangeTextDay}
                            underlineColorAndroid="transparent"
                            ref="txtdayinput"
                            style={[{ width: this.columnWidth }]}
                        />
                    </View>
                    <View style={layout.floatGroup}>
                        <FloatLabelTextField
                            placeholder="Reward point"
                            value=""
                            onChangeTextValue={this.onChangeTextreward}
                            underlineColorAndroid="transparent"
                            ref="txtrewardinput"
                        />
                    </View>
                    <View style={styles.btnSave}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.btnSaveWraper}
                            onPress={this.saveClient}
                        >
                            <LinearGradient
                                start={[0, 0]}
                                end={[1, 0]}
                                colors={["#F069A2", "#EEAEA2"]}
                                style={styles.btnLinear}
                            >
                                <Text style={styles.btnSaveText}>{this.getText('clientsavelbl')}</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <SubmitLoader
                        ref="clientLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={this.getText('processing')}
                        color={Colors.spinnerLoaderColorSubmit}
                    />
                <IconLoader
                    ref="clientSuccessLoader"
                    visible={false}
                    textStyle={layout.textLoaderScreenSubmitSucccess}
                    textContent={this.getText('clientsavedlbl')}
                    color={Colors.spinnerLoaderColorSubmit}
                />
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    twocolumn: {
        flexDirection: "row",
        flexWrap: "wrap"
    },
    seperatecolumn: {
        borderWidth: 1,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderColor: "#ddd",
        backgroundColor: "red",
        height: 45
    },
    btnSave: {
        height: 45,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 16,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 15,
        right: 15
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1
    }
});
