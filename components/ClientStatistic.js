import React from "react";
import { StyleSheet, WebView, Text, View, Platform, Modal, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo";
import layout from "../assets/styles/layout";
import NavigationBarBackground from "../components/navigationBarBG";
import NavigationBarTitle from "../components/navigationBarTitle";
import SpinnerLoader from "../helpers/spinner";
import Colors from "../constants/Colors";
import setting from "../constants/Setting";
import { isLogged, jwtToken, getUserData } from "../helpers/authenticate";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default class ClientStatistic extends React.Component {
    state = {
        modalVisible: this.props.visible
    };

    clientId = 0;
    clientName = '';

    close() {   
        this.setState({ modalVisible: false });
    }

    show = () => {
        this.setState({ modalVisible: true });
    };

    renderLoading = () => {
        return (
            <View style={styles.container}>
                <SpinnerLoader
                    visible={true}
                    textStyle={layout.textLoaderScreen}
                    overlayColor={"transparent"}
                    textContent={"Loading..."}
                    color={Colors.spinnerLoaderColor}
                />
            </View>
        );
    };

    async componentWillMount() {
        this.token = await jwtToken();
    }

    render() {
       

        return (
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}
            >
                <View
                    style={
                        Platform.OS === "android"
                            ? layout.headercontainerAndroid
                            : layout.headercontainer
                    }
                >
                    <LinearGradient
                        start={[0, 0]}
                        end={[1, 0]}
                        colors={["#F069A2", "#EEAEA2"]}
                        style={
                            Platform.OS === "android"
                                ? layout.headerAndroid
                                : layout.header
                        }
                    >
                        <View style={layout.headercontrols}>
                            <TouchableOpacity
                                style={layout.headerNavLeftContainer}
                                activeOpacity={1}
                                onPress={() => this.close()}
                            >
                                <View style={layout.headerNavLeft}>
                                    <MaterialCommunityIcons
                                        name={"close"}
                                        size={20}
                                        color={"rgba(255,255,255,1)"}
                                        style={
                                                        Platform.OS ===
                                                        "android"
                                                            ? layout.navIcon
                                                            : layout.navIconIOS
                                                    }
                                    />
                                </View>
                            </TouchableOpacity>
                            <View
                                style={{
                                    flex: 1,
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                            >
                                
                                <Text style={layout.headertitle}>
                                    {this.clientName}
                                </Text>
                            </View>
                            
                        </View>
                    </LinearGradient>
                </View>

                <View style={styles.container}>
                    <WebView
                        domStorageEnabled={true}
                        startInLoadingState={true}
                        javaScriptEnabled={true}
                        renderLoading={this.renderLoading}
                        source={{
                            uri: setting.clientStatisticUrl + '?clientid=' + this.clientId,
                            headers: { Authorization: "Bearer " + this.token }
                        }}
                        style={styles.container}
                    />
                </View>                     
                
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
