import React from "react";
import { StyleSheet, Text, View, TouchableOpacity,Dimensions, Keyboard, Alert } from "react-native";
import {
    inputCurrency
} from "../../helpers/Utils";
import { LinearGradient } from "expo";
import FloatLabelTextInput from "../FloatTextInput";
import FloatLabelTextField from "../FloatTextInput";
import { getTextByKey } from "../../helpers/language";
import { formatPhone } from "../../helpers/Utils";
import emailvalidator from "email-validator";
import ClientSearchModal from "../ClientSearchModal";
import setting from "../../constants/Setting";
import layout from "../../assets/styles/layout";
import Colors from "../../constants/Colors";
import IconLoader from "../../helpers/iconloader";
import SubmitLoader from "../../helpers/submitloader";
var width = Dimensions.get('window').width;
var amountWidth = (width - 30) / 5;
export default class GiftAmount extends React.Component {
    state = {
        valueOther: false
    }

    provider = this.props.provider;
    amount = 0;
    ApproxValue = 0;
    clientName = "";
    selectedClient = 0;
    isClientPhone = false;
    isValidClient = false;
    clients = this.props.clients;
    recipientname = "";
    recipientemail = "";
    onChangeBtnCCAmount = value => {
        value = inputCurrency(value);
        this.amount = parseFloat(value.replace('$',''));
        let ApproxValue = parseFloat(value.replace('$',''));
        if(this.provider.percentPlusForGiftCard > 0){ 
            this.ApproxValue  = ApproxValue + (ApproxValue * this.provider.percentPlusForGiftCard / 100);
        }else{
            this.ApproxValue = 0;
        }
        this.props.onInputAmount(this.amount);
        this.setState({ valueOther: false});
    };
    getText(key){
        return getTextByKey(this.props.language,key);
    }
    clearForm = () => {
        this.amount = 0;
        this.ApproxValue = 0;
        this.clientName = "";
        this.selectedClient = 0;
        this.isClientPhone = false;
        this.isValidClient = false;
        this.recipientname = "";
        this.recipientemail = "";
        state = {
            valueOther: false
        }
    }
    otheramount=()=>{
        this.ApproxValue = 0;
        this.amount = 0;
        this.amountshow = '';
        this.setState({valueOther:true});
    }

    onChangeTextCCAmount = value => {
        value = inputCurrency(value);
        this.amountshow = value;
        this.amount = parseFloat(value.replace('$',''));
        let ApproxValue = parseFloat(value.replace('$',''));
        if(this.provider.percentPlusForGiftCard > 0 && this.amount){ 
            this.ApproxValue  = ApproxValue + (ApproxValue * this.provider.percentPlusForGiftCard / 100);
        }else{
            this.ApproxValue = 0;
        }
        this.props.onInputAmount(this.amount);
        this.refs.ccamountinput.setState({ text: value });
        this.setState({rerender:true});
    };
    onChangeTextRecipientName = (value) => {
        this.refs.RecipientName.setState({ text: value });
        this.recipientname = value;
    }

    onChangeTextRecipientEmail = (value) => {
        this.refs.RecipientEmail.setState({ text: value });
        this.recipientemail = value;
    }

    refreshClients = async () => {

        var clientList = await fetch(setting.apiUrl + 'get_clients', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.props.token,
            }
        }).then((response) => response.json()).then((responseJson) => {

            if (responseJson.success) {
                AsyncStorage.setItem('list-client', JSON.stringify(responseJson.data));
                return responseJson.data;
            } else {
                Alert.alert('Error', responseJson.message);
                return [];
            }
        }).catch((error) => {
            return [];
        });
        
        if(clientList.length){
            this.clients = clientList;
        }

        this.setTextClient(this.clientName);
    }
    setTextClient = (value) => {

        value = String.prototype.trim.call(value);
        value = value.replace('(','');
        value = value.replace(')','');
        value = value.replace(' ','');
        value = value.replace('-','');
        let isPhone = false;
        if(value.length >= 3 && !isNaN(value)){
            let formatValue = formatPhone(value);
            this.refs.clientInput.setState({text: formatValue});
            isPhone = true;
            value = formatValue.replace(/[^\d]+/g, '');
        }
        this.clientName = value;
        if(value.length >= 4){
            let clientsFiltered = [];
            let isActive = false;
            if(isPhone){
                clientsFiltered = this.clients.filter(function(item){
                    let phone = '';
                    if (typeof item.phone != 'undefined' && item.phone != '' && item.phone != null) {
                        phone = item.phone.replace(/[^\d]+/g, '');
                    }
                    return phone.indexOf(value) >= 0;
                });
                isActive = value.length == 10;
            }else{
                clientsFiltered = this.props.clients.filter(function(item){
                    return item.email.toLowerCase().indexOf(value.toString().toLowerCase()) >= 0;
                });
                isActive = emailvalidator.validate(String.prototype.trim.call(value));
            }
            this.isClientPhone = isPhone;
            this.isValidClient = isActive;

            this.refs.clientlistmodal.show(clientsFiltered.slice(0, 5),isPhone,true, isActive,value);
        }else{
            this.isClientPhone = false;
            this.clientName = '';
            this.isValidClient = false;
            this.refs.clientlistmodal.show([],false,false,false,value);
        }
    }
    _onSelectedClient = (client, lbl) => {
        this.refs.clientInput.setState({text: lbl});
        Keyboard.dismiss();
        this.refs.clientlistmodal.show([],false,false, false,'');
        this.selectedClient = client.id;
    }
    createAsNewClient = (value) => {
        this.clientName = value;
        this.selectedClient = 0;
        this.refs.clientlistmodal.show([],false,false, false,'');
    }
    async savesell(){
        var isvalid = true;
        if(this.selectedClient == 0){
            Alert.alert('Error', "Please select client");
            isvalid = false;
        }else if(this.amount == 0){
            Alert.alert('Error', "Please select amount");
            isvalid = false;
        }else if(this.props.type == "giveagift"){
            if(String.prototype.trim.call(this.recipientname) == ''){
                Alert.alert('Error','Please enter Recipient\'s name');
                isvalid = false;
            }else if(String.prototype.trim.call(this.recipientemail) == ''){
                Alert.alert('Error','Please enter Recipient\'s email');
                isvalid = false;
            }else if(!emailvalidator.validate(String.prototype.trim.call(this.recipientemail))){
                Alert.alert('Error', 'Please enter a valid email');
                isvalid = false;
            }
        }
        if(isvalid){
            this.refs.saveLoader.setState({ visible: true });
            //call login api
           let isSuccess = await fetch(setting.apiUrl + 'sell/egift',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: "Bearer " + this.props.token
                },
                body: JSON.stringify({
                    amount: this.amount,
                    clientid: this.selectedClient,
                    type: this.props.type,
                    recipientname:this.recipientname,
                    recipientemail:this.recipientemail  
                })
            }).then((response) => response.json()).then((responseJson) => {
                if(!responseJson.success)
                {
                    let _this = this;
                    _this.refs.saveLoader.setState({ visible: false });  
                    return false;
                }else
                {
                    let _this = this;
                    _this.refs.saveLoader.setState({ visible: false });   
    
                    let successMessage = "Save success";
                    _this.refs.saveSuccessLoader.setState({
                        textContent: successMessage,
                        visible: true
                    });
    
                    setTimeout(function() {
                        _this.refs.saveSuccessLoader.setState({
                            visible: false
                        });
                        _this.refs.clientInput.setState({text: ""});
                        _this.clearForm();
                        _this.props.onPay();
                        _this.setState({ valueOther: false });
    
                    }, 3000);
 
                    return true; 
                }
                
            }).catch((error) => {
                console.error(error);
            });
             if(!isSuccess){
                //Alert.alert('Error', "Fail");
             }
        }
    };
        

    render() {

        return (
            <View>
                <View>
                    <View style={styles.clientrow}>
                        <Text style={styles.clientlbl}>Choose amount</Text>
                        
                        <View style={{  flexDirection:'row',marginTop:15}}>
                            <TouchableOpacity onPress={() => {this.onChangeBtnCCAmount('$10')}} 
                                style={[styles.btn_amount,this.amount == 10 && !this.state.valueOther ? styles.active : false,{borderTopLeftRadius: 4, borderBottomLeftRadius: 4}]}>
                                <Text style={styles.amounttxt}>$10</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.onChangeBtnCCAmount('$30')}}
                                style={[styles.btn_amount,styles.btn_amount_on,this.amount == 30 && !this.state.valueOther ? styles.active : false ]}>
                                <Text style={styles.amounttxt}>$30</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.onChangeBtnCCAmount('$50')}} 
                                style={[styles.btn_amount,styles.btn_amount_on,this.amount == 50 && !this.state.valueOther ? styles.active : false ]}>
                                <Text style={styles.amounttxt}>$50</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.onChangeBtnCCAmount('$100')}} 
                                style={[styles.btn_amount,styles.btn_amount_on,this.amount == 100 && !this.state.valueOther ? styles.active : false ]}>
                                <Text style={styles.amounttxt}>$100</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.otheramount()}} 
                                style={[styles.btn_amount,styles.btn_amount_on, this.state.valueOther ? styles.active : false,{borderTopRightRadius: 4, borderBottomRightRadius: 4} ]}>
                                <Text style={styles.amounttxt}>Other</Text>
                            </TouchableOpacity>
                        </View>
                            {
                                this.state.valueOther == true &&
                                <View style={{marginTop:10}}>
                                    <FloatLabelTextField
                                        placeholder={"Other Amount"}
                                        value={this.amountshow}
                                        onChangeTextValue={this.onChangeTextCCAmount}
                                        underlineColorAndroid="transparent"
                                        ref="ccamountinput"
                                    />
                                </View>
                            }
                            
                    </View>
                </View> 
                {
                    this.ApproxValue > 0 && 
                    <View style={[styles.columnWraperLeft]}>
                        <View style={styles.clientrow}>
                            <Text style={styles.clientlbl}>Approx Value: <Text style={{color:'red'}}>${this.ApproxValue}</Text></Text>
                        </View>
                    </View>
                }
                <View style={styles.blockseperate}></View>
                <View style={styles.clientrow2}>
                    <Text style={styles.clientlbl1}>Choose client</Text>
                    <FloatLabelTextInput
                            placeholder={getTextByKey(this.props.language,'clientphoneoremail')}
                            value={this.clientName}
                            onChangeTextValue={(value) => this.setTextClient(value)}
                            ref="clientInput"
                        />
                    <ClientSearchModal
                    ref={"clientlistmodal"}
                    onSelectedClient={this._onSelectedClient}
                    createAsNewClient={this.createAsNewClient}
                    token={this.props.token}
                    refresh={async () => { await this.refreshClients() }}
                    language={this.props.language}
                />
                </View>
                { this.props.type == "giveagift" &&
                <View style={{zIndex:-1}}>
                    <View style={styles.blockseperate}></View>
                    <View style={{paddingTop:15}}>
                        <Text style={styles.clientlbl1}>Recipient Info</Text>
                        <View style={[layout.floatGroup,{marginTop:5,marginBottom:5}]}>
                            <FloatLabelTextInput
                                placeholder={"Recipient's name"}
                                value={this.recipientname}
                                onChangeTextValue={this.onChangeTextRecipientName}
                                underlineColorAndroid="transparent"
                                ref="RecipientName"
                            />
                        </View>
                        <View style={[layout.floatGroup,{marginTop:5,marginBottom:5}]}>
                            <FloatLabelTextInput
                                placeholder={"Recipient's email"}
                                value={this.recipientemail}
                                onChangeTextValue={this.onChangeTextRecipientEmail}
                                underlineColorAndroid="transparent"
                                ref="RecipientEmail"
                            />
                        </View>
                    </View>
                </View>
                }


                <TouchableOpacity
                    style={styles.btnLogout}
                    activeOpacity={1}
                    onPress={async () => await this.savesell()}
                >
                <LinearGradient
                    start={[0, 0]}
                    end={[1, 0]}
                    colors={[
                    "#F069A2",
                    "#EEAEA2"]}
                    style={styles.btnLinear}>
                    <Text style={styles.btnLogoutText}>
                        Submit
                    </Text>
                </LinearGradient>
                </TouchableOpacity>
                <SubmitLoader
                        ref="saveLoader"
                        visible={false}
                        textStyle={layout.textLoaderScreenSubmit}
                        textContent={this.getText('processing')}
                        color={Colors.spinnerLoaderColorSubmit}
                    />
                <IconLoader
                    ref="saveSuccessLoader"
                    visible={false}
                    textStyle={layout.textLoaderScreenSubmitSucccess}
                    textContent="Sell Success"
                    color={Colors.spinnerLoaderColorSubmit}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    clientrow:{
        backgroundColor:'#fff',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:0,
        paddingBottom:15,
        position:'relative'
    },   
    clientrow2:{
        backgroundColor:'#fff',
        paddingTop:15,
        paddingBottom:5,
        position:'relative'
    }, 
     clientlbl:{
        flexDirection:'row',
        fontSize: 18
    },
    clientlbl1:{
        flexDirection:'row',
        fontSize: 18,
        paddingLeft:15,
        paddingRight:15,
        zIndex:-1
    },
    btn_amount:{
        borderColor: "#ddd",
        padding:10,
        borderWidth: 1,
        width: amountWidth,
        alignItems:'center'
    },
    btn_amount_on:{
        borderLeftWidth:0
    },
    amounttxt:{
        fontSize:16
    },
    active:{
        backgroundColor:'#f2f2f2'
    },
    blockseperate:{
        height:10,
        backgroundColor:'#f2f2f2',
        zIndex:-1
    },
    btnLogout: {
        height:50,
        backgroundColor:'#fff',
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginTop: 25,
        borderWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd',
        zIndex:-1
    },
    btnLogoutText:{
        color:'#fff',
        fontSize: 18
    },
        btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1,
        width:"100%"
    },
});
