import React from "react";
import { StyleSheet, Text, View, TouchableOpacity,Dimensions,Alert, Keyboard, ScrollView, UIManager, TextInput, SectionList } from "react-native";
import moment from "moment";

export default class EgiftSold extends React.Component {

    state = {
        appIsReady: false
    };
    datalist = [];
    componentWillMount() {
        data = {};
        data.key = "header";
        data.data = this.props.data;
        this.datalist.push(data);
    }
    RefreshData = (datarefresh) =>{
        this.datalist = [];
        data = {};
        data.key = "header";
        data.data = datarefresh;
        this.datalist.push(data);
        this.setState({appIsReady: true});
    }
    _keyExtractor = (item, index) => "egiftsold-"+item.id;
    _renderSectionHeader = sectionHeader => {
        return (
            <View style={styles.sectionheadertext}>
                <View style={styles.sectionheadertextcontainer}>
                    <Text style={styles.sectionheadertextcontent}>
                        Type
                    </Text>
                </View>
                <View style={styles.sectionheadertextcontainer}>
                    <Text style={styles.sectionheadertextcontent}>
                        Buyer
                    </Text>
                </View>
                <View style={styles.sectionheadertextcontainer}>
                    <Text style={styles.sectionheadertextcontent}>
                    Receiver
                    </Text>
                </View>
            </View>
        );
    };
    _renderItem = ({ item }) => {  
        let typename = "";
        let Channel = "";
        let name = "";
        let emailbuyer = "";
        let phonebuyer = "";
        if(item.senderId != "" && item.senderId != null && item.senderId > 0){
             typename = "Buy for Friend";
             namebuyer = item.sender_firstname + ' ' + item.sender_lastname;
             emailbuyer = item.sender_email;
             phonebuyer = item.sender_phone;
        }else{
             typename = "Buy for Me";
             namebuyer = item.buyer_firstname + ' ' + item.buyer_lastname;
             emailbuyer = item.buyer_email;
             phonebuyer = item.buyer_phone;
        }
        if(item.token == "sell-e-gift"){
           Channel= "In Salon";
        }else{
            Channel= "Online"; 
        }


        return (
            <View>
            <View style={styles.clientrow}>
                <View style={[styles.bookingrow,{width: 150}]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>{typename}</Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <Text style={[styles.clientlbl]}>Channel: {Channel}</Text>
                    </View>
                    {
                        item.senderId != "" && item.senderId != null && item.senderId > 0 &&
                        <View style={{justifyContent: 'center'}}>
                            <Text style={[styles.clientlbl]}>Code: {item.redeemCode}</Text>
                        </View>
                    }

                    { item.senderId != "" && item.senderId != null && item.senderId > 0 && item.clientid > 0 &&
                        <View style={{justifyContent: 'center'}}>
                            <Text style={[styles.clientlbl, styles.colorused]}>Used</Text>
                        </View>
                    }

                </View>
                <View style={[styles.bookingrow,{marginRight:30, width: 150}]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Payment</Text>
                    </View>

                    <View >
                        <Text style={styles.clientlbl}>Price: ${item.price}</Text>
                        <Text style={styles.clientlbl}>Date: {moment(item.dateofpayment).format('MMM DD YYYY')}</Text>
                    </View>
                    
                </View>

                <View style={[styles.bookingrow1,{marginRight:30}]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Buyer</Text>
                    </View>
                    <View >
                        <Text style={styles.clientlbl}>{namebuyer} </Text>
                        <Text style={styles.clientlbl}>{phonebuyer}</Text>
                        <Text style={styles.clientlbl}>{emailbuyer}</Text>
                    </View>
                </View>
                
                <View style={[styles.bookingrow1,{marginRight:30}]}>
                    <View>
                        <Text style={[styles.clientlbl, styles.colortime]}>Receiver</Text>
                    </View>
                    { item.senderId != "" && item.senderId != null && item.senderId > 0 &&
                    <View >
                        <Text style={styles.clientlbl}>{item.recipientname}</Text>
                        <Text style={styles.clientlbl}>{item.recipientemail}</Text>
                    </View>
                    }
                </View>



            </View>
            <View style={[styles.line]}></View>
            </View>
            
        );
    };
    render() {
        return (
            <SectionList
            ref="egiftsold"
            renderItem={this._renderItem}
            keyExtractor={this._keyExtractor}
            sections={this.datalist}
        />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainer:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle:{
        color:'#fff',
        backgroundColor:'transparent',
        fontSize:22,
        fontFamily:'Futura',
        marginTop:10
    },
    row:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    booksuccesstext:{
        fontFamily:'Futura',
        fontSize:22,
        color:'#333',
        textAlign:'center',
        marginTop:10
    },
    confirmbtn:{
        justifyContent: "center",
        alignItems: "center",
        width: 350
    },
    btnSave: {
        height: 45,
        width: 230,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginBottom: 15
    },
    btnSaveText: {
        color: "#fff",
        fontSize: 20,
        zIndex: 1,
        backgroundColor: "transparent"
    },
    btnSaveWraper: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    btnLinear: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: "hidden",
        flex: 1,
    },
   
    columnWraperLeft:{
        backgroundColor:'#fff',
        borderWidth: 0.5,
        borderColor: '#ddd',
        marginBottom:15,
    },
    clientrow:{
        flexDirection:'row',
        backgroundColor:'#fff',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:15,
        paddingBottom:15,
        position:'relative',
        // justifyContent: 'center',
        // alignItems: 'center',
        // justifyContent: 'space-between',
    },
    bookingrow:{
        position:'relative', 
    },
    bookingrow1:{
        position:'relative', 
        flex: 3,
        paddingLeft:10
    },
    line:{
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ddd',
        position:'relative'
    },
    clientlblheader:{
        fontSize:22,
  
        // textTransform: 'uppercase',

    },
    clientlbl:{
        fontSize:16,
        marginBottom:5
    },
    settings:{
        position: 'absolute',
        zIndex: 1,
        left: 10,
        top:40,
        alignItems: 'center', 
        justifyContent: 'center', 
        color:"#fff"
    },
    btnbooknow:{
        color:'#fff',
        textAlign:'center',
        fontSize:10,
        padding:5, 

      },
      colortime:{
          color:'#757373'
      },
      colorused:{
        color:'#F069A2'
    },
      
      righticon:{
          position:'absolute',
          right:0,
          top:8
      },
      removeservice:{
    
        backgroundColor:'red',
        justifyContent:'center',
        paddingRight:10,
        position:'absolute',
        alignItems: "center",
        width:100,
        height:79
    },
    removeservicebtnText:{
        color:'#fff'
    },
});
