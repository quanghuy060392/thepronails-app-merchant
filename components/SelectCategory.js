import React from "react";
import {StyleSheet, Text, TouchableOpacity, View, Modal, Platform, TextInput, FlatList } from "react-native";
import BlockedTimeTechnicianSearchItem from "./BlockedTimeTechnicianSearchItem";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {LinearGradient} from "expo";
import layout from "../assets/styles/layout";
import moment from "moment";
import "../helpers/timezone";
import { getUSState2Digit, get_time_zone } from "../helpers/Utils";
import collect from "collect.js";
import { getTextByKey } from "../helpers/language";

export default class SelectCategory extends React.Component {
    state = {
        modalVisible: false,
        showCloseSearchBoxClient: false,
        search:'',
        selected: this.props.selected,
    }
    Category = this.props.data;
    close() {
        this.setState({modalVisible: false});
    }

    show = (techid) => {
        this.setState({modalVisible: true});
    }

    _keyExtractor = (item, index) => item.id;

    _onPressItem = (id, name) => {
        this.props.onSelected(id,name);
    };

    getText(key){
        return getTextByKey(this.props.language,key);
    }

    _renderItem = ({item}) => {
        if( (typeof this.state.search == 'undefined' || (this.state.search != 'undefined' && item.name.toLowerCase().indexOf(this.state.search.toLowerCase()) >= 0)))
        {
            return (
                <TouchableOpacity activeOpacity={1} onPress={() => this._onPressItem(item.id, item.name)}>
                <View style={styles.itemContainer}>
                    <Text style={[styles.technicianname, {color:(item.id == this.state.selected ? '#F069A2' : '#6b6b6b')}]}>
                        {item.name}
                    </Text>
                </View>
            </TouchableOpacity>
            )
        }

    };

    clearSearchClient = () => {
        this.refs['searchtextinput'].clear();
        this.setState({showCloseSearchBoxClient: false});
        //this.state.search = '';
        this.setState({search: ''});
    }

    changeSearchTextClient = (searchtext) => {
        if (String.prototype.trim.call(searchtext) == '') {
            this.setState({showCloseSearchBoxClient: false});
        } else {
            this.setState({showCloseSearchBoxClient: true});
        }
        //console.log(searchtext);
        this.setState({search: searchtext});
    }

    render() {
        //console.log(this.props.data);
        return(
            <Modal
                animationType={"none"}
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.close()}

            >
                <View style={(Platform.OS === 'android' ? layout.headercontainerAndroid : layout.headercontainer )}>
                    <LinearGradient start={[0, 0]} end={[1, 0]} colors={['#F069A2', '#EEAEA2']}
                                    style={( Platform.OS === 'android' ? layout.headerAndroid : layout.header)}>
                        <View style={layout.headercontrols}>
                            <TouchableOpacity style={layout.headerNavLeftContainer} activeOpacity={1}
                                              onPress={() => this.close()}>
                                <View style={layout.headerNavLeft}>
                                    <MaterialCommunityIcons
                                        name={'close'}
                                        size={30}
                                        color={'rgba(255,255,255,1)'} style={(Platform.OS === 'android' ? layout.navIcon : layout.navIconIOS)}
                                    />
                                </View>
                            </TouchableOpacity>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={layout.headertitle}>{this.getText('selecttechnicianappointmentsearch')}</Text>
                            </View>
                        </View>
                    </LinearGradient>
                </View>
                <View >
                    <View style={layout.searchContainer}>
                        <MaterialCommunityIcons
                            name={'magnify'}
                            size={20}
                            color={'#6b6b6b'} style={layout.iconsearchbox}
                        />
                        <TextInput
                            placeholder={this.getText('selecttechnicianappointmentsearchtxt')} placeholderTextColor='#6b6b6b'
                            underlineColorAndroid={'transparent'}
                            style={layout.searchbox}
                            onChangeText={(searchtext) => this.changeSearchTextClient(searchtext)}
                            ref={'searchtextinput'}
                        />

                        {this.state.showCloseSearchBoxClient &&
                        <TouchableOpacity style={layout.iconclosesearchbox} activeOpacity={1}
                                          onPress={() => this.clearSearchClient()}>
                            <MaterialCommunityIcons
                                name={'close-circle-outline'}
                                size={20}
                                color={'#6b6b6b'}
                            />
                        </TouchableOpacity>
                        }


                    </View>
                </View>
                <View style={{flex:1}}>
                    <FlatList
                        data={this.Category}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                        extraData={(this.state )}
                        initialNumToRender={10}
                    />
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemContainer: {
        height:50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#CFD4DA',
        borderBottomWidth: 0.5,
        paddingLeft:10,
        paddingRight:10
    },
    technicianname:{
        marginLeft:5,
        color:'#6b6b6b',
        fontSize:16
    },
    technicianturn:{
        color:'red',
        fontSize:16,
        marginLeft:5
    },
    technicianserving:{
        color:'#fff',
        fontSize:16,
        marginLeft:5,
        backgroundColor:'blue',
        paddingTop:2,
        paddingBottom:2,
        paddingLeft:5,
        paddingRight:5
    },
    technicianstatus:{
        color:'#7878',
        fontSize:16,
        marginLeft:5,
        
        paddingTop:2,
        paddingBottom:2,
        paddingLeft:5,
        paddingRight:5
    }
});
