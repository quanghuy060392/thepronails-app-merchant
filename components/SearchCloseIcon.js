import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import layout from "../assets/styles/layout";

export default class SearchCloseIcon extends React.Component {
    state = {
        visible: this.props.visible
    };

    _onPress = () => {
        this.props.onPress();
    };

    render() {
        if (this.state.visible) {
            return (
                <TouchableOpacity
                    style={layout.iconclosesearchbox}
                    activeOpacity={1}
                    onPress={this._onPress}
                >
                    <MaterialCommunityIcons
                        name={"close-circle-outline"}
                        size={20}
                        color={"#6b6b6b"}
                    />
                </TouchableOpacity>
            );
        }else return false
    }
}
